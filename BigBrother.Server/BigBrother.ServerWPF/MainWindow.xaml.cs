﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BigBrother.Server.Library;

namespace BigBrother.ServerWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        /// <summary>
        /// Поле хоста службы
        /// </summary>
        private ServiceHost _host;

        /// <summary>
        /// Поле объекта служьы
        /// </summary>
        static ScreenService service = null;

        static BrowserCheck browserCheck = null;

        private static WriteableBitmap prevImage;
        /// <summary>
        /// Форма
        /// </summary>
     

        public static string GroupUser { get; set; }

        private static string FIO { get; set; }
        public MainWindow()
        {
            string hostName = Environment.MachineName;
            string userName = Environment.UserName;

         /*   Task checkProccess = new Task(() =>
            {

                browserCheck.CheckProcces();
            });
            checkProccess.Start();*/
            Task task1 = new Task(() =>
            {
                service = new ScreenService();

                service.ConnectionEvent += new EventHandler<ConnectionEventArgs>(service_ConnectionEvent);
                service.GetImagesEvent += new EventHandler<ConnectionEventArgs>(service_ImagesEvent);
                service.GetAccess += new EventHandler<ConnectionEventArgs>(service_GetAccess);



                using (ServiceHost _host = new ServiceHost(service,
                         new Uri($"net.tcp://{hostName}:8090"),
                            new Uri($"http://{hostName}:8080")))
                {
                    NetTcpBinding binding = new NetTcpBinding()
                    {
                        MaxBufferSize = 2147483647,
                        MaxReceivedMessageSize = 2147483647
                    };
                    binding.Security.Mode = SecurityMode.None;
                    _host.AddServiceEndpoint(typeof(IScreen), binding, "ScreenService");
                    ServiceMetadataBehavior mex =
_host.Description.Behaviors.Find<ServiceMetadataBehavior>();
                    if (mex == null)
                    {
                        mex = new ServiceMetadataBehavior { HttpGetEnabled = true };
                        _host.Description.Behaviors.Add(mex);
                    }
                    _host.AddServiceEndpoint(typeof(IMetadataExchange),
                    MetadataExchangeBindings.CreateMexHttpBinding(),
                    "mex");
                    ServiceDiscoveryBehavior discoveryBehavior = new ServiceDiscoveryBehavior();
                    _host.Description.Behaviors.Add(discoveryBehavior);
                    _host.AddServiceEndpoint(new UdpDiscoveryEndpoint());
                    _host.Opening += new EventHandler(host_Opening);
                    _host.Opened += new EventHandler(host_Opened);
                    try
                    {
                        _host.Open();
                        //GetAd();

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                    }
                    Console.ReadLine();
                }
            });
            task1.Start();

            /*
            Task task2 = new Task(() =>
            {

                Pc pc;
                Group group;
                User user ;
                Thread.Sleep(20000);
                using (LibraryContext context = new LibraryContext())
                {
                    DB DB = new DB(context);
                    pc = DB.GetPcOrAdd(hostName);
                    group = DB.GetGroupOrAdd(GroupUser);
                    user = DB.GetUserOrAdd(userName, FIO, group);
                }
                List<string> SessionSite = new List<string>();
                while (true)
                {
                    foreach (AutomationElement ae in AE)
                    {
                        if (ae != null) {
                            try
                            {
                                string site =
                                    ae.GetCurrentPropertyValue(ValuePatternIdentifiers.ValueProperty).ToString();
                                if (!site.Equals(""))
                                {
                                    if (!SessionSite.Contains(site))
                                    {
                                        SessionSite.Add(site);
                                        using (LibraryContext context = new LibraryContext())
                                        {
                                            DB DB = new DB(context);
                                            DB.AddActivitiesUser(pc, site, user);
                                        }
                                        Console.WriteLine(site);
                                    }
                                }
                            }
                            catch (ElementNotAvailableException)
                            {
                                
                            }
                        }
                    }
                    Thread.Sleep(5000);
                }
            });
            task2.Start();
              
           */
            InitializeComponent();
        }


        static void host_Opening(object sender, EventArgs e)
        {
            Console.WriteLine("Opening service.......");
        }

        static void host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("Service is ready!\nPress any key to terminate service...");
        }

        static void service_ConnectionEvent(object sender, ConnectionEventArgs e)
        {
            Console.WriteLine(e.Ip.Select(u => u.Ip));
        }

        /// <summary>
        /// Обработчик события получения изображения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void service_ImagesEvent(object sender, ConnectionEventArgs e)
        {
            BitmapImage partial;
            Int32Rect bounds;
            byte[] imgData;
            Win32Stuff.RESULTION resultion;
            if (e.ImageBytes != null)
            {

                Function.UnpackImage(e.ImageBytes, out partial, out bounds, out imgData, out resultion);

                Function.UpdateScreen(ref prevImage, partial, bounds, imgData, resultion);
                System.Windows.Application.Current.Dispatcher.Invoke((Action) (() =>
                {
                    image.Source = prevImage;
                }));
                GC.Collect();
            }
        }
        /// <summary>
        /// Обработчик события получения разрешения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void service_GetAccess(object sender, ConnectionEventArgs e)
        {

            if (e.Access)
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    okno.Visibility = Visibility.Visible;
                    okno.ShowInTaskbar = true;
            }));
        }
            else
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    okno.Visibility = Visibility.Hidden;
                okno.ShowInTaskbar = false;
                }));
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BigBrother.Server.Library
{
    [DataContract]
    public class UserInfo
    {
        [DataMember]
        public string Ip { get; set; }

        /// <summary>
        /// Переопределенный метод Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return obj != null && this.ToString() == obj.ToString();
        }

        /// <summary>
        /// Переопределенный метод GetHashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Переопределенный метод ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Ip;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Cursor = System.Windows.Forms.Cursor;

namespace BigBrother.Server.Library
{

    public class KeyOperations
    {
        private const byte KEYUP = 0x2;
        private const byte KEYDOWN = 0x0;
        private const byte VK_SHIFT = 0x10;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern short VkKeyScan(char ch);

        public static void SendKey(char key, byte vbyte = 0x00)
        {
            byte Vcode = (byte)VkKeyScan(key);
 
            if (vbyte != 0x00)
            {
                keybd_event(vbyte, 0, KEYDOWN, 0);
                keybd_event(Vcode, 0, KEYDOWN, 0);
                keybd_event(Vcode, 0, KEYUP, 0);
                keybd_event(vbyte, 0, KEYUP, 0);
            }
            else
            {
                keybd_event(Vcode, 0, KEYDOWN, 0);
                keybd_event(Vcode, 0, KEYUP, 0);
            }
        }
        public static void SendKey(byte key, byte vbyte = 0x00)
        {
            if (vbyte != 0x00)
            {
                keybd_event(vbyte, 0, KEYDOWN, 0);
                keybd_event(key, 0, KEYDOWN, 0);
                keybd_event(key, 0, KEYUP, 0);
                keybd_event(vbyte, 0, KEYUP, 0);
            }
            else
            {
                keybd_event(key, 0, KEYDOWN, 0);
                keybd_event(key, 0, KEYUP, 0);
            }

        }

        /// <summary>
        /// Функция для получения значения байта для специфичных клавиш
        /// </summary>
        /// <param name="Key">Клавиша клавиатуры</param>
        /// <returns></returns>


        public static byte getByteCode(string Key)
        {
            byte dataByte = 0x00;
            switch (Key)
            {
                case "back":
                    dataByte = 0x08;
                    break;
                case "lwin":
                    dataByte = 0x5B;
                    break;
                case "rwin":
                    dataByte = 0x5C;
                    break;
                case "escape":
                    dataByte = 0x1B;
                    break;
                case "numpad0":
                    dataByte = 0x60;
                    break;
                case "numpad1":
                    dataByte = 0x61;
                    break;
                case "numpad2":
                    dataByte = 0x62;
                    break;
                case "numpad3":
                    dataByte = 0x63;
                    break;
                case "numpad4":
                    dataByte = 0x64;
                    break;
                case "numpad5":
                    dataByte = 0x65;
                    break;
                case "numpad6":
                    dataByte = 0x66;
                    break;
                case "numpad7":
                    dataByte = 0x67;
                    break;
                case "numpad8":
                    dataByte = 0x68;
                    break;
                case "numpad9":
                    dataByte = 0x69;
                    break;
                case "multiply":
                    dataByte = 0x6A;
                    break;
                case "add":
                    dataByte = 0x6B;
                    break;
                case "subtract":
                    dataByte = 0x6D;
                    break;
                case "decimal":
                    dataByte = 0x6E;
                    break;
                case "divide":
                    dataByte = 0x6F;
                    break;
                case "return":
                    dataByte = 0x0D;
                    break;
                case "space":
                    dataByte = 0x20;
                    break;
                case "up":
                    dataByte = 0x26;
                    break;
                case "left":
                    dataByte = 0x25;
                    break;
                case "right":
                    dataByte = 0x27;
                    break;
                case "down":
                    dataByte = 0x28;
                    break;
                case "numlock":
                    dataByte = 0x90;
                    break;
                case "capital":
                    dataByte = 0x14;
                    break;
                case "scroll":
                    dataByte = 0x91;
                    break;
                case "f1":
                    dataByte = 0x70;
                    break;
                case "f2":
                    dataByte = 0x71;
                    break;
                case "f3":
                    dataByte = 0x72;
                    break;
                case "f4":
                    dataByte = 0x73;
                    break;
                case "f5":
                    dataByte = 0x74;
                    break;
                case "f6":
                    dataByte = 0x75;
                    break;
                case "f7":
                    dataByte = 0x76;
                    break;
                case "f8":
                    dataByte = 0x77;
                    break;
                case "f9":
                    dataByte = 0x78;
                    break;
                case "oemminus":
                    dataByte = 0xBD;
                    break;
                case "oemplus":
                    dataByte = 0xBB;
                    break;
                case "oem3":
                    dataByte = 0xC0;
                    break;
                case "leftshift":
                    dataByte = 0xA0;
                    break;
                case "rightshift":
                    dataByte = 0xA1;
                    break;
                case "leftctrl":
                    dataByte = 0xA2;
                    break;
                case "rightctrl":
                    dataByte = 0xA3;
                    break;
                case "insert":
                    dataByte = 0x2D;
                    break;
                case "home":
                    dataByte = 0x24;
                    break;
                case "pageup":
                    dataByte = 0x21;
                    break;
                case "delete":
                    dataByte = 0x2E;
                    break;
                case "end":
                    dataByte = 0x23;
                    break;
                case "next":
                    dataByte = 0x22;
                    break;
                case "tab":
                    dataByte = 0x09;
                    break;
                case "oem5":
                    dataByte = 0xDC;
                    break;
                case "oem1":
                    dataByte = 0xBA;
                    break;
                case "oemquotes":
                    dataByte = 0xDE;
                    break;
                case "oemquestion":
                    dataByte = 0xBF;
                    break;
                case "oemopenbrackets":
                    dataByte = 0xDB;
                    break;
                case "oem6":
                    dataByte = 0xDD;
                    break;
                case "oem8":
                    dataByte = 0xDF;
                    break;
                case "oemcomma":
                    dataByte = 0xBC;
                    break;
                case "oemperiod":
                    dataByte = 0xBE;
                    break;
                case "d0":
                    dataByte = 0x30;
                    break;
                case "d1":
                    dataByte = 0x31;
                    break;
                case "d2":
                    dataByte = 0x32;
                    break;
                case "d3":
                    dataByte = 0x33;
                    break;
                case "d4":
                    dataByte = 0x34;
                    break;
                case "d5":
                    dataByte = 0x35;
                    break;
                case "d6":
                    dataByte = 0x36;
                    break;
                case "d7":
                    dataByte = 0x37;
                    break;
                case "d8":
                    dataByte = 0x38;
                    break;
                case "d9":
                    dataByte = 0x39;
                    break;
                case "system":
                    dataByte = 0x12;
                    break;
            }
            return dataByte;
        }
    }
    public class MouseOperations
    {
        [Flags]
        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010,
            MouseWhell = 0x0800
        }

        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int X, int Y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out MousePoint lpMousePoint);

        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public static void SetCursorPosition(int X, int Y)
        {
            SetCursorPos(X, Y);
        }

        public static void SetCursorPosition(MousePoint point)
        {
            SetCursorPos(point.X, point.Y);
        }

        public static MousePoint GetCursorPosition()
        {
            MousePoint currentMousePoint;
            var gotPoint = GetCursorPos(out currentMousePoint);
            if (!gotPoint) { currentMousePoint = new MousePoint(0, 0); }
            return currentMousePoint;
        }

        public static void MouseEvent(MouseEventFlags value)
        {
            MousePoint position = GetCursorPosition();

            mouse_event
                ((int)value,
                 position.X,
                 position.Y,
                 0,
                 0)
                ;
        }

        public static void MouseEvent(MouseEventFlags value, int delta)
        {
            MousePoint position = GetCursorPosition();
            mouse_event
                 ((int)value,
                position.X,
                  position.Y,
                  delta,
                  0);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePoint
        {
            public int X;
            public int Y;

            public MousePoint(int x, int y)
            {
                X = x;
                Y = y;
            }

        }

    }

    public class CommandInfo
    {
        public enum CommandTypeOption { MouseMove, MouseLeftUpClick, MouseLeftDownClick, MouseRightUpClick, MouseRightDownClick, MouseWhell, KeyUp, LogOff, ShutDown, Restart, Message};
        public CommandTypeOption CommandType { get; set; }
        public string Data { get; set; }

        public CommandInfo(CommandTypeOption type, string data)
        {
            CommandType = type;
            Data = data;
        }

        public override string ToString()
        {
            return CommandType + "|" + Data;
        }

        public static CommandInfo Parse(string input)
        {
            string[] parts = input.Split('|');
            if (parts.Length != 2)
            {
                return null;
            }
            CommandTypeOption type = (CommandTypeOption)Enum.Parse(typeof(CommandTypeOption), parts[0]);
            string data = parts[1];
            return new CommandInfo(type, data);
        }
    }

    public class CommandInfoCollection
    {
        private readonly Queue<CommandInfo> _cmds = new Queue<CommandInfo>();
        public string SerializeCommandStack()
        {
            StringBuilder bld = new StringBuilder();
            lock (_cmds)
            {
                foreach (CommandInfo cmd in _cmds)
                {
                    bld.Append(cmd.ToString());
                    bld.Append(";");
                }
                _cmds.Clear();
            }

            return bld.ToString();
        }
        public void DeserializeCommandStack(string input)
        {
            lock (_cmds)
            {
                string[] parts = input.Split(';');
                foreach (string part in parts)
                {
                    string trimmedPart = part.Trim();
                    if (!string.IsNullOrEmpty(trimmedPart))
                    {
                        CommandInfo cmd = CommandInfo.Parse(trimmedPart);
                        if (cmd != null)
                        {
                            _cmds.Enqueue(cmd);
                        }
                    }
                }
            }
        }

        public void Add(CommandInfo.CommandTypeOption type, string data)
        {
            CommandInfo cmd = new CommandInfo(type, data);
            Add(cmd);
        }

        public void Add(CommandInfo cmd)
        {
            lock (_cmds)
            {
                _cmds.Enqueue(cmd);
            }
        }

        public CommandInfo GetNextCommand()
        {
            CommandInfo cmd = null;
            lock (_cmds)
            {
                try
                {
                    cmd = _cmds.Dequeue();
                }
                catch
                {
                    // Do something with the exception
                };
            }

            return cmd;
        }
    }

    public static class Command
    {
        public static void Execute(CommandInfo cmd)
        {
            switch (cmd.CommandType)
            {
                case CommandInfo.CommandTypeOption.MouseLeftUpClick:
                    MouseLeftUpClick();
                    break;
                case CommandInfo.CommandTypeOption.MouseLeftDownClick:
                    MouseLeftDownClick();
                    break;
                case CommandInfo.CommandTypeOption.MouseRightDownClick:
                    MouseRightDownClick();
                    break;
                case CommandInfo.CommandTypeOption.MouseRightUpClick:
                    MouseRightUpClick();
                    break;
                case CommandInfo.CommandTypeOption.MouseMove:
                    MouseMove(cmd.Data);
                    break;
                case CommandInfo.CommandTypeOption.MouseWhell:
                    MouseWhell(Convert.ToInt32(cmd.Data));
                    break;
                case CommandInfo.CommandTypeOption.KeyUp:
                    KeyUp(cmd.Data);
                    break;
                case CommandInfo.CommandTypeOption.LogOff:
                    LogOffPc();
                    break;
                case CommandInfo.CommandTypeOption.Restart:
                    Restart();
                    break;
                case CommandInfo.CommandTypeOption.ShutDown:
                    ShutDown();
                    break;
                case CommandInfo.CommandTypeOption.Message:
                    Message(cmd.Data);
                    break;
            }
        }

        private static void MouseLeftUpClick()
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
        }

        private static void MouseLeftDownClick()
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
        }

        private static void MouseRightDownClick()
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.RightDown);
        }

        private static void MouseRightUpClick()
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.RightUp);
        }

        private static void MouseWhell(int delta)
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.MouseWhell, delta);
        }

        private static void LogOffPc()
        {
            Process.Start("cmd.exe", "/C" + "shutdown -l -f");
        }

        private static void ShutDown()
        {
            Process.Start("cmd.exe", "/C" + "shutdown -s -f");
        }

        private static void Restart()
        {
            Process.Start("cmd.exe", "/C" + "shutdown -r -f");
        }

        private static void Message(string message)
        {
            Task task1 = new Task(() => { 
                 MessageBox.Show(message);
        });
               task1.Start();

            
        }

        private static void KeyUp(string data)
        {
            string[] parts = data.Split('|');
            if (parts.Length != 2)
            {
                return;
            }
            data = parts[0];
            byte vbyte = byte.Parse(parts[1]);
            if (vbyte == 0x00)
            {
                byte dataByte = KeyOperations.getByteCode(data);
                if (dataByte == 0x00)
                {
                    KeyOperations.SendKey(data[0]);
                }
                else
                {
                    KeyOperations.SendKey(dataByte);
                }
            }
            else
            {
                byte dataByte = KeyOperations.getByteCode(data);
                if (dataByte == 0x00)
                {
                    KeyOperations.SendKey(data[0], vbyte);
                }
                else
                {
                    KeyOperations.SendKey(dataByte, vbyte);
                }
            }
            Console.WriteLine($"Key: {data}, KEY: {vbyte}");
        }

        private static void MouseMove(string data)
        {
            string[] parts = data.Split(',');
            if (parts.Length != 2)
            {
                return;
            }
            int cursorX = int.Parse(parts[0]);
            int cursorY = int.Parse(parts[1]);

            Cursor.Position = new System.Drawing.Point(cursorX, cursorY);
        }
    }
}
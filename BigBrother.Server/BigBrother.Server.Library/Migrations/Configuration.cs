namespace BigBrother.Server.Library.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BigBrother.Server.Library.DataModel.LibraryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "BigBrother.Server.Library.DataModel.LibraryContext";
        }

        protected override void Seed(BigBrother.Server.Library.DataModel.LibraryContext context)
        {

        }
    }
}

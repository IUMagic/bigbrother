namespace BigBrother.Server.Library.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataBase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivitiesUsers",
                c => new
                    {
                        ActivitiesUserId = c.Int(nullable: false, identity: true),
                        Url = c.String(nullable: false, unicode: false),
                        DateTime = c.DateTime(nullable: false, precision: 0),
                        IsBlock = c.Boolean(nullable: false),
                        UserId = c.Int(),
                        PcId = c.Int(),
                    })
                .PrimaryKey(t => t.ActivitiesUserId)
                .ForeignKey("dbo.Pcs", t => t.PcId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.PcId);
            
            CreateTable(
                "dbo.Pcs",
                c => new
                    {
                        PcId = c.Int(nullable: false, identity: true),
                        PcName = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.PcId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 255, storeType: "nvarchar"),
                        Fio = c.String(maxLength: 255, storeType: "nvarchar"),
                        GroupId = c.Int(),
                        RoleId = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Groups", t => t.GroupId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.GroupId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        GroupId = c.Int(nullable: false, identity: true),
                        GroupName = c.String(nullable: false, maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.GroupId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        RoleName = c.String(nullable: false, maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.BlockSites",
                c => new
                    {
                        BlockSiteId = c.Int(nullable: false, identity: true),
                        Url = c.String(nullable: false, unicode: false),
                        CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.BlockSiteId)
                .ForeignKey("dbo.CategoryBlocks", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.CategoryBlocks",
                c => new
                    {
                        CategoryBlockId = c.Int(nullable: false, identity: true),
                        Category = c.String(nullable: false, maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.CategoryBlockId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlockSites", "CategoryId", "dbo.CategoryBlocks");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Users", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.ActivitiesUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.ActivitiesUsers", "PcId", "dbo.Pcs");
            DropIndex("dbo.BlockSites", new[] { "CategoryId" });
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Users", new[] { "GroupId" });
            DropIndex("dbo.ActivitiesUsers", new[] { "PcId" });
            DropIndex("dbo.ActivitiesUsers", new[] { "UserId" });
            DropTable("dbo.CategoryBlocks");
            DropTable("dbo.BlockSites");
            DropTable("dbo.Roles");
            DropTable("dbo.Groups");
            DropTable("dbo.Users");
            DropTable("dbo.Pcs");
            DropTable("dbo.ActivitiesUsers");
        }
    }
}

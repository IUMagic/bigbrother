﻿using System;
using BigBrother.Server.Library.DataModel;
using System.Linq;
using System.ServiceModel.Security;

namespace BigBrother.Server.Library
{
    public class DB
    {
        private readonly LibraryContext _context = null;
        public DB(LibraryContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Получить или добавить пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="fio">Фамилия пользователя</param>
        /// <param name="group">Id группы</param>
        /// <returns>полученный или созданный объект</returns>
        public User GetUserOrAdd(string userName, string fio, Group group)
        {
            User user = _context.Users.Where(c => c.Login.Equals(userName)).Select(c => c).FirstOrDefault()
                                           ?? _context.Users.Add(new User
                                           {
                                               Fio = fio,
                                               Login = userName,
                                               RoleId = 1,
                                               GroupId = group.GroupId

                                           });

            _context.SaveChanges();
            return user;
        }

        /// <summary>
        /// Получить или добавить группу
        /// </summary>
        /// <param name="groupName">Название группы</param>
        /// <returns>Полученный или созданный объект</returns>
        public Group GetGroupOrAdd(string groupName)
        {
            Group group = _context.Groups.Where(c => c.GroupName.Equals(groupName)).Select(c => c).FirstOrDefault()
                          ?? _context.Groups.Add(new Group
                          {
                              GroupName = groupName
                          });
            _context.SaveChanges();
            return group;
        }

        /// <summary>
        /// Получить или добавить компьютер
        /// </summary>
        /// <param name="pcName">Название компьютера</param>
        /// <returns>Полученный или созданный объект</returns>
        public Pc GetPcOrAdd(string pcName)
        {
            Pc pc = _context.Pcs.Where(c => c.PcName.Equals(pcName)).Select(c => c).FirstOrDefault()
                     ?? _context.Pcs.Add(new Pc
                       {
                            PcName = pcName
                       });
            _context.SaveChanges();
            return pc;
        }

        /// <summary>
        /// Добавить активность пользователя в браузеере
        /// </summary>
        /// <param name="pc">Id PC</param>
        /// <param name="Url">Ссылка на сайт</param>
        /// <param name="user">Id User</param>
        public void AddActivitiesUser(Pc pc, string Url, User user)
        {
            ActivitiesUser activitiesUser = new ActivitiesUser
            {
                DateTime = DateTime.Now,
                IsBlock = false,
                PcId = pc.PcId,
                Url = Url,
                UserId = user.UserId
            };
            _context.ActivitiesUsers.Add(activitiesUser);
            _context.SaveChanges();
        }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BigBrother.Server.Library.DataModel
{
    public class CategoryBlock
    {
        public int CategoryBlockId { get; set; }
        [Required]
        [MaxLength(255)]
        public string Category { get; set; }
        
         public ICollection<BlockSite> BlockSites { get; set; }

        public CategoryBlock()
        {
            BlockSites = new List<BlockSite>();
        } 
    }
}
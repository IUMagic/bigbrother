﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BigBrother.Server.Library.DataModel
{
    public class ActivitiesUser
    {
        public int ActivitiesUserId { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public DateTime DateTime { get; set; }
        [Required]
        public bool IsBlock { get; set; }

        public int? UserId { get; set; }
        public int? PcId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        [ForeignKey("PcId")]
        public virtual Pc Pc { get; set; }
    }
}
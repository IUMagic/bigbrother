﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBrother.Server.Library.DataModel
{
    public class User
    {
        public int UserId { get; set; }
        [Required]
        [MaxLength(255)]
        public string Login { get; set; }
        [MaxLength(255)]
        public string Fio { get; set; }

        public int? GroupId { get; set; }

        public int? RoleId { get; set; }

        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }

        public ICollection<ActivitiesUser> ActivitiesUsers { get; set; }

        public User()
        {
            ActivitiesUsers = new List<ActivitiesUser>();
        } 
    }
}

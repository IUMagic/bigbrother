﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BigBrother.Server.Library.DataModel
{
    public class BlockSite
    {
         public int BlockSiteId { get; set; }
        [Required]
        public string Url { get; set; }
        public int? CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual CategoryBlock CategoryBlock { get; set; }
    }
}
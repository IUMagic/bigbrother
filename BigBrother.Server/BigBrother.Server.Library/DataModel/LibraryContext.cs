﻿namespace BigBrother.Server.Library.DataModel
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class LibraryContext : DbContext
    {

        public LibraryContext()
            : base("name=LibraryContext")
        {
        }

        public DbSet<Group> Groups { get; set; } 
        public DbSet<Role> Roles { get; set; }
        public DbSet<BlockSite> BlockSites { get; set; }
        public DbSet<CategoryBlock> CategoryBlocks  {get;set;}
        public DbSet<ActivitiesUser> ActivitiesUsers  {get;set;}
        public DbSet<User> Users  {get;set;}
        public DbSet<Pc> Pcs  {get;set;}
        }
        
    }

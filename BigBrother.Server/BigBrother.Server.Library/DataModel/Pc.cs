﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BigBrother.Server.Library.DataModel
{
    public class Pc
    {
         public int PcId { get; set; }
        [Required]
        [MaxLength(100)]
        public string PcName { get; set; }

        public ICollection<ActivitiesUser> ActivitiesUsers { get; set; }

        public Pc()
        {
            ActivitiesUsers = new List<ActivitiesUser>();
        } 
    }
}
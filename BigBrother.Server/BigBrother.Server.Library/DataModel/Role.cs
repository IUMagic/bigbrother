﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BigBrother.Server.Library.DataModel
{
    public class Role
    {
        public int RoleId { get; set; }
        [MaxLength(255)]
        [Required]
        public string RoleName { get; set; }

        public ICollection<User> Users { get; set; }

        public Role()
        {
            Users = new List<User>();
        } 
    }
}
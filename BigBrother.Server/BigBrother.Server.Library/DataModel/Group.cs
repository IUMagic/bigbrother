﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BigBrother.Server.Library.DataModel
{
    public class Group
    {
         public int GroupId { get; set; }
        [MaxLength(255)]
        [Required]
        public string GroupName { get; set; }

        public ICollection<User> Users { get; set; }

        public Group()
        {
            Users = new List<User>();
        } 
    }
}
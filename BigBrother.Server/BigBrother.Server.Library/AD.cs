﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;

namespace BigBrother.Server.Library
{
    public class AD
    {
        #region Переменные

        readonly string sDomain = "iat.iat";
        readonly string sDefaultOU = "OU=test,DC=domain,DC=local";
        readonly string sDefaultRootOU = "DC=iat,DC=iat";
        protected string sServiceUser;
        protected string sServicePassword;

        public AD(string username, string password)
        {
            sServiceUser = username;
            sServicePassword = password;
        }
        public AD()
        {
        }

        #endregion


        /// <summary>
        /// Проверка имени пользователя и пароля
        /// </summary>
        /// <param name="sUserName">Имя пользователя</param>
        /// <param name="sPassword">Пароль</param>
        /// <returns>Возвращает true, если имя и пароль верны</returns>
        public bool ValidateCredentials(string sUserName, string sPassword)
        {
            return GetPrincipalContext().ValidateCredentials(sUserName, sPassword);
        }

        /// <summary>
        /// Получить указанного пользователя Active Directory
        /// </summary>
        /// <param name="sUserName">Имя пользователя для извлечения</param>
        /// <returns>Объект UserPrincipal</returns>
        public UserPrincipal GetUser(string sUserName)
        {
            PrincipalContext oPrincipalContext = GetPrincipalContext();
            return UserPrincipal.FindByIdentity(oPrincipalContext, IdentityType.SamAccountName, sUserName);
        }


        /// <summary>
        /// Возвращает список групп, в которых состоит пользователь
        /// </summary>
        /// <param name="sUserName">Имя пользователя</param>
        /// <returns>Возвращает List со всеми группами пользователя</returns>
        public List<string> GetUserGroups(string sUserName)
        {
            List<string> myItems = new List<string>();
            UserPrincipal oUserPrincipal = GetUser(sUserName);

            using (PrincipalSearchResult<Principal> oPrincipalSearchResult = oUserPrincipal.GetGroups())
            {
                foreach (Principal oResult in oPrincipalSearchResult)
                {
                    myItems.Add(oResult.Name);
                }
            }

            return myItems;
        }    

        /// <summary>
        /// Возвращает последнюю группу пользователя
        /// </summary>
        /// <param name="sUserName">Имя пользователя</param>
        /// <returns>Возвращает последнюю групу пользователя</returns>
        public string GetUserLastGroup(string sUserName)
        {
            UserPrincipal oUserPrincipal = GetUser(sUserName);

            using (PrincipalSearchResult<Principal> oPrincipalSearchResult = oUserPrincipal.GetGroups())
            {
                var lastOrDefault = oPrincipalSearchResult.LastOrDefault();
                if (lastOrDefault != null) return lastOrDefault.Name;
            }
            return null;
        }



        /// <summary>
        /// Получить базовый основной контекст
        /// </summary>
        /// <returns>Возвращает объект PrincipalContext</returns>
        public PrincipalContext GetPrincipalContext()
        {

            return new PrincipalContext(ContextType.Domain, sDomain, sDefaultRootOU, null,null);
        }

    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Point = System.Windows.Point;
using Color = System.Windows.Media.Color;
using Size = System.Windows.Size;
namespace BigBrother.Server.Library
{


    /// <summary>
    /// ���������� ��������� ������
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]

    public class ScreenService : IScreen
    {
        private Bitmap _prevImage; 
        private Bitmap _newBitmap = new Bitmap(1,1);
        private bool _prevIsOne;
        private Graphics _graphics;
        Rectangle bounds = Rectangle.Empty;
        const int numBytesPerPixel = 4;
        private Win32Stuff.RESULTION resoulution;
        /// <summary>
        /// ������� ���������� � ����������� ������ ������������(��������� ����������)
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ConnectionEvent = delegate { };
        /// <summary>
        /// ������� ���������� � ��������� ����������� � �������
        /// </summary>
        public event EventHandler<ConnectionEventArgs> GetImagesEvent = delegate { };
        /// <summary>
        /// ������� ���������� � ��������� ����������
        /// </summary>
        public event EventHandler<ConnectionEventArgs> GetAccess = delegate { }; 

        /// <summary>
        /// ��������� (�������) ��� �������� ���������� � ������������ ������������� � ������� ��������� ������
        /// </summary>
        public readonly Dictionary<UserInfo, IScreenCallback> CallbackStorage =
            new Dictionary<UserInfo, IScreenCallback>();

        /// <summary>
        /// ��������� ��� ��������� �����������, ���������� ����� WinAPI
        /// </summary>
        public struct SIZE
        {
            public int Cx;
            public int Cy;
        }

        /// <summary>
        /// ���������� �������� ����������� ������ ������������
        /// </summary>
        /// <param name="ip">Ip �������</param>
        /*public void Connect(UserInfo ip)
        {
            if (CallbackStorage.ContainsKey(ip)) return;
            IScreenCallback callback = OperationContext.Current.GetCallbackChannel<IScreenCallback>();
            CallbackStorage.Add(ip, callback);

            foreach (IScreenCallback cb in CallbackStorage.Values)
            {
                cb.GetCoonectedPc(CallbackStorage.Keys.ToArray());
            }

            ConnectionEventArgs args = new ConnectionEventArgs(CallbackStorage.Keys.ToArray());
            OnConnectionEvent(args);
        }*/

        /// <summary>
        /// ���������� ��������� ��������� � �������
        /// </summary>
        /// <param name="access">���������� �� ��������� ��������� �������</param>
        /// <param name="imageBytes">������ ������ ���������</param>
        public void AccessForTranslation(bool access)
        {
            IScreenCallback callback = OperationContext.Current.GetCallbackChannel<IScreenCallback>();
            try
            {
                callback.ResponseImageClient("aye: " + access);
            }
            catch (CommunicationException)
            {
                
            }
            ConnectionEventArgs args = new ConnectionEventArgs(access);
            OnGetAccess(args);
        }
        /// <summary>
        /// ��������� ����������� � �������
        /// </summary>
        /// <param name="imageBytes"></param>
        public void GetScreenShotForServer(byte[] imageBytes)
        {
            IScreenCallback callback = OperationContext.Current.GetCallbackChannel<IScreenCallback>();
            try
            {
                callback.ResponseImageClient("aye");
            }
            catch (CommunicationException)
            {
                
            }
            ConnectionEventArgs args = new ConnectionEventArgs(imageBytes);
            OnGetImagesEvent(args);

        }


        /// <summary>
        /// ���������� ���������� ������������
        /// </summary>
        /// <param name="ip">Ip �������</param>
        /*public void Disconnect(UserInfo ip)
        {
            if (!CallbackStorage.ContainsKey(ip)) return;
            IScreenCallback callback = OperationContext.Current.GetCallbackChannel<IScreenCallback>();
            CallbackStorage.Remove(ip);

            foreach (IScreenCallback cb in CallbackStorage.Values)
            {
                cb.GetCoonectedPc(CallbackStorage.Keys.ToArray());
            }

            ConnectionEventArgs args = new ConnectionEventArgs(CallbackStorage.Keys.ToArray());
            OnConnectionEvent(args);
        }*/

        /// <summary>
        /// ���������� ��������� � ������� ���������
        /// </summary>
        public void GetScreenShot(int indexPc, bool isOne)
        {
            int cursorX = 0;
            int cursorY = 0;
            IScreenCallback callback = OperationContext.Current.GetCallbackChannel<IScreenCallback>();
            if (isOne != _prevIsOne)
            {
                _prevImage = null;
            }
            _prevIsOne = isOne;
            Bitmap b = Function.GetScreenBitmap(ref bounds, ref resoulution);
            
            if (b != null && bounds != Rectangle.Empty)
            {
                byte[] data = Function.PackImage(b, bounds, resoulution);

                try
                {
                    callback.ResponseImageServer(data, indexPc, isOne);
                }
                catch (CommunicationException)
                {
                    Debug.Write("error");
                }
            }
            if (isOne == false)
            {
                Bitmap image = Function.GetCursorBitmap(ref cursorX, ref cursorY);
           
                if (image != null)
                {
                    byte[] data = Function.PackCursor(image, cursorX, cursorY);

                    try
                    {
                        callback.ResponseCursorServer(data);
                    }
                    catch (CommunicationException)
                    {
                        Debug.Write("error");
                    }
                }
            }
            GC.Collect();
            
        }

        /// <summary>
        /// ���������� ��������� � ������� ��� ������������
        /// </summary>
        public void GetUserName()
        {
            IScreenCallback callback = OperationContext.Current.GetCallbackChannel<IScreenCallback>();
            try
            {
                callback.ResponseUserName(SystemInformation.UserName);
            }
            catch (CommunicationException)
            {
                
            }
        }


        /// <summary>
        /// ���������� ���������
        /// </summary>
        /// <returns>Bitmap</returns>
        /*
        Bitmap GetScreenBitmap()
        {
            Bitmap screenShot = new Bitmap(SystemInformation.VirtualScreen.Width,
                                         SystemInformation.VirtualScreen.Height,
                                         System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics screenGraph = Graphics.FromImage(screenShot);
            screenGraph.CopyFromScreen(SystemInformation.VirtualScreen.X,
                                       SystemInformation.VirtualScreen.Y,
                                       0,
                                       0,
                                       SystemInformation.VirtualScreen.Size,
                                       CopyPixelOperation.SourceCopy);
            return screenShot;
        }
        */

 

        /// <summary>
        /// ���������� ���������
        /// </summary>
        /// <returns>Bitmap</returns>

   
        public void updateCursor(string data)
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.MouseMove, data);
            Command.Execute(cmd);
        }

        public void MouseLeftUpClick()
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.MouseLeftUpClick, "");
            Command.Execute(cmd);
        }

        public void MouseLeftDownClick()
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.MouseLeftDownClick, "");
            Command.Execute(cmd);
        }

        public void MouseRightDownClick()
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.MouseRightDownClick, "");
            Command.Execute(cmd);
        }

        public void MouseRightUpClick()
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.MouseRightUpClick, "");
            Command.Execute(cmd);
        }

        public void MouseWheell(int delta)
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.MouseWhell, delta.ToString());
            Command.Execute(cmd);
        }

        public void KeyUp(string key, byte vbyte)
        {
            string data = key + "|" + vbyte;
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.KeyUp, data);
            Command.Execute(cmd);
        }

        public void ShutDown()
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.ShutDown, "");
            Command.Execute(cmd);
        }

        public void Restart()
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.Restart, "");
            Command.Execute(cmd);
        }

        public void LogOff()
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.LogOff, "");
            Command.Execute(cmd);
        }

        public void SendMessage(string message)
        {
            CommandInfo cmd = new CommandInfo(CommandInfo.CommandTypeOption.Message, message);
            Command.Execute(cmd);
        }





        /// <summary>
        /// ����� ��� ������ ������� ���������� � ����������� ������ ������������ (��������� ����������)
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnConnectionEvent(ConnectionEventArgs e)
        {
            ConnectionEvent?.Invoke(this, e);
        }

        /// <summary>
        /// ����� ��� ������ ������� ���������� � ��������� ����������� � �������
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnGetImagesEvent(ConnectionEventArgs e)
        {
            GetImagesEvent?.Invoke(this, e);
        }

        protected virtual void OnGetAccess(ConnectionEventArgs e)
        {
            GetAccess?.Invoke(this, e);
        }

 
    }
}
﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Point = System.Windows.Point;

namespace BigBrother.Server.Library
{
    public class Function
    {
        private static Bitmap _prevImage;
        private static Bitmap _newBitmap = new Bitmap(1, 1);
        private static Graphics _graphics;
        static Rectangle bounds = Rectangle.Empty;
        const int numBytesPerPixel = 4;
        private  static Win32Stuff.RESULTION resoulution;
        public static Bitmap GetScreenBitmap(ref Rectangle bounds, ref Win32Stuff.RESULTION screen_resolution)
        {
            Bitmap diff = null;

            _newBitmap = ScreenCapture.GetScreenshot();
            screen_resolution = new Win32Stuff.RESULTION
            {
                width = _newBitmap.Width,
                height = _newBitmap.Height
            };
            if (_prevImage != null)
            {
                bounds = GetChangeScreenshot();
                if (bounds != Rectangle.Empty)
                {
                    diff = new Bitmap(bounds.Width, bounds.Height);
                    _graphics = Graphics.FromImage(diff);
                    _graphics.DrawImage(_newBitmap, 0, 0, bounds, GraphicsUnit.Pixel);

                    _prevImage = _newBitmap;


                }
            }
            else
            {
                _prevImage = _newBitmap;
                diff = _newBitmap;

                // Создаем ограничивающий прямоугольник.
                //
                bounds = new Rectangle(0, 0, _newBitmap.Width, _newBitmap.Height);
            }
            return diff;
        }

        private static Rectangle GetChangeScreenshot()
        {
            if (_prevImage.Width != _newBitmap.Width || _prevImage.Height != _newBitmap.Height || _prevImage.PixelFormat != _newBitmap.PixelFormat) return Rectangle.Empty;

            int width = _newBitmap.Width;
            int height = _newBitmap.Height;
            int left = width;
            int right = 0;
            int top = height;
            int bottom = 0;

            BitmapData bmNewData = null;
            BitmapData bmPrevData = null;

            try
            {
                bmNewData = _newBitmap.LockBits(new Rectangle(0, 0, _newBitmap.Width, _newBitmap.Height),
                    ImageLockMode.ReadOnly, _newBitmap.PixelFormat);
                bmPrevData = _prevImage.LockBits(new Rectangle(0, 0, _prevImage.Width, _prevImage.Height),
                    ImageLockMode.ReadOnly, _prevImage.PixelFormat);

                int strideNew = bmNewData.Stride / numBytesPerPixel;
                int stridePrev = bmPrevData.Stride / numBytesPerPixel;

                IntPtr scanNew0 = bmNewData.Scan0;
                IntPtr scanPrev0 = bmPrevData.Scan0;

                unsafe
                {
                    int* pNew = (int*)(void*)scanNew0;
                    int* pPrev = (int*)(void*)scanPrev0;

                    for (int y = 0; y < _newBitmap.Height; ++y)
                    {
                        for (int x = 0; x < left; ++x)
                        {
                            if ((pNew + x)[0] != (pPrev + x)[0])
                            {
                                if (x < left)
                                {
                                    left = x;
                                }
                                if (x > right)
                                {
                                    right = x;
                                }
                                if (y < top)
                                {
                                    top = y;
                                }
                                if (y > bottom)
                                {
                                    bottom = y;
                                }
                            }
                        }
                        pNew += strideNew;
                        pPrev += stridePrev;
                    }

                    if (left != width)
                    {
                        pNew = (int*)(void*)scanNew0;
                        pPrev = (int*)(void*)scanPrev0;
                        pNew += (_newBitmap.Height - 1) * strideNew;
                        pPrev += (_prevImage.Height - 1) * stridePrev;


                        for (int y = _newBitmap.Height - 1; y > top; y--)
                        {

                            for (int x = _newBitmap.Width - 1; x > right; x--)
                            {

                                if ((pNew + x)[0] != (pPrev + x)[0])
                                {

                                    if (x > right)
                                    {
                                        right = x;
                                    }
                                    if (y > bottom)
                                    {
                                        bottom = y;
                                    }
                                }
                            }


                            pNew -= strideNew;
                            pPrev -= stridePrev;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                if (bmPrevData != null)
                {
                    _prevImage.UnlockBits(bmPrevData);
                }
                if (bmNewData != null)
                {
                    _newBitmap.UnlockBits(bmNewData);
                }
            }

            int diffImgWIdth = right - left + 1;
            int diffImgHeight = bottom - top + 1;

            if (diffImgHeight < 0 || diffImgWIdth < 0)
            {
                return Rectangle.Empty;
            }
            return new Rectangle(left, top, diffImgWIdth, diffImgHeight);
        }

        public static byte[] PackImage(Image image, Rectangle bounds, Win32Stuff.RESULTION resultion)
        {
            byte[] imgData;

            using (MemoryStream ms = new MemoryStream())
            {
                /*   ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);

                   // Create an Encoder object based on the GUID  
                   // for the Quality parameter category.  
                   System.Drawing.Imaging.Encoder myEncoder =
                       System.Drawing.Imaging.Encoder.Quality;

                   // Create an EncoderParameters object.  
                   // An EncoderParameters object has an array of EncoderParameter  
                   // objects. In this case, there is only one  
                   // EncoderParameter object in the array.  
                   EncoderParameters myEncoderParameters = new EncoderParameters(1);

                   EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 20L);
                   myEncoderParameters.Param[0] = myEncoderParameter;
                   image.Save(ms, jpgEncoder, myEncoderParameters);*/
                image.Save(ms, ImageFormat.Png);
                imgData = ms.ToArray();
            }

            byte[] topData = BitConverter.GetBytes(bounds.Top);
            byte[] bottomData = BitConverter.GetBytes(bounds.Bottom);
            byte[] leftData = BitConverter.GetBytes(bounds.Left);
            byte[] rightData = BitConverter.GetBytes(bounds.Right);
            byte[] width = BitConverter.GetBytes(resultion.width);
            byte[] height = BitConverter.GetBytes(resultion.height);
            int sizeTopData = topData.Length;

            byte[] result = new byte[imgData.Length + 6 * sizeTopData];
            Array.Copy(topData, 0, result, 0, topData.Length);
            Array.Copy(bottomData, 0, result, sizeTopData, bottomData.Length);
            Array.Copy(leftData, 0, result, 2 * sizeTopData, leftData.Length);
            Array.Copy(rightData, 0, result, 3 * sizeTopData, rightData.Length);
            Array.Copy(width, 0, result, 4 * sizeTopData, width.Length);
            Array.Copy(height, 0, result, 5 * sizeTopData, height.Length);
            Array.Copy(imgData, 0, result, 6 * sizeTopData, imgData.Length);


            return result;
        }

        public static byte[] PackCursor(Image image, int cursorX, int cursorY)
        {
            byte[] imgData;
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Png);
                imgData = ms.ToArray();
            }

            byte[] xPos = BitConverter.GetBytes(cursorX);
            byte[] yPos = BitConverter.GetBytes(cursorY);

            byte[] result = new byte[imgData.Length + xPos.Length + yPos.Length];

            Array.Copy(xPos, 0, result, 0, xPos.Length);
            Array.Copy(yPos, 0, result, xPos.Length, yPos.Length);
            Array.Copy(imgData, 0, result, xPos.Length + yPos.Length, imgData.Length);

            return result;
        }

        public static void UnpackCursor(byte[] data, out BitmapImage bitmap, out int cursorX, out int cursorY)
        {
            const int numBytesInInt = sizeof(int);

            int imgLength = data.Length - 2 * numBytesInInt;

            byte[] xPos = new byte[numBytesInInt];
            byte[] yPos = new byte[numBytesInInt];
            byte[] imgData = new byte[imgLength];

            Array.Copy(data, 0, xPos, 0, numBytesInInt);
            Array.Copy(data, numBytesInInt, yPos, 0, numBytesInInt);
            Array.Copy(data, 2 * numBytesInInt, imgData, 0, imgLength);

            cursorX = BitConverter.ToInt32(xPos, 0);
            cursorY = BitConverter.ToInt32(yPos, 0);

            using (MemoryStream ms = new MemoryStream(imgData, 0, imgData.Length))
            {
                ms.Write(imgData, 0, imgData.Length);

                bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = ms;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
            }
        }

        public static void UnpackImage(byte[] data, out Image image, out Rectangle bounds)
        {
            const int numBytesInInt = sizeof(int);
            int imgLength = data.Length - 6 * numBytesInInt;

            byte[] topPosData = new byte[numBytesInInt];
            byte[] bottomPosData = new byte[numBytesInInt];
            byte[] leftPosData = new byte[numBytesInInt];
            byte[] rightPosData = new byte[numBytesInInt];
            byte[] widthData = new byte[numBytesInInt];
            byte[] heightData = new byte[numBytesInInt];
            byte[] imgData = new byte[imgLength];

            Array.Copy(data, 0, topPosData, 0, numBytesInInt);
            Array.Copy(data, numBytesInInt, bottomPosData, 0, numBytesInInt);
            Array.Copy(data, 2 * numBytesInInt, leftPosData, 0, numBytesInInt);
            Array.Copy(data, 3 * numBytesInInt, rightPosData, 0, numBytesInInt);
            Array.Copy(data, 4 * numBytesInInt, widthData, 0, numBytesInInt);
            Array.Copy(data, 5 * numBytesInInt, heightData, 0, numBytesInInt);
            Array.Copy(data, 6 * numBytesInInt, imgData, 0, imgLength);

            // Создаем растровое изображение из массива байтов.
            //
            MemoryStream ms = new MemoryStream(imgData, 0, imgData.Length);
            ms.Write(imgData, 0, imgData.Length);
            image = Image.FromStream(ms, true);

            // Создаем связанный прямоугольник.
            //
            int top = BitConverter.ToInt32(topPosData, 0);
            int bottom = BitConverter.ToInt32(bottomPosData, 0);
            int left = BitConverter.ToInt32(leftPosData, 0);
            int right = BitConverter.ToInt32(rightPosData, 0);

            int width = right - left + 1;
            int height = bottom - top + 1;
            
         
            bounds = new Rectangle(left, top, width, height);

        }

        public static void UnpackImage(byte[] data, out BitmapImage bitmap, out Int32Rect bounds, out byte[] imgData, out Win32Stuff.RESULTION resultion)
        {
            const int numBytesInInt = sizeof(int);

            int imgLength = data.Length - 6 * numBytesInInt;

            byte[] topPosData = new byte[numBytesInInt];
            byte[] bottomPosData = new byte[numBytesInInt];
            byte[] leftPosData = new byte[numBytesInInt];
            byte[] rightPosData = new byte[numBytesInInt];
            byte[] widthData = new byte[numBytesInInt];
            byte[] heightData = new byte[numBytesInInt];
            imgData = new byte[imgLength];

            Array.Copy(data, 0, topPosData, 0, numBytesInInt);
            Array.Copy(data, numBytesInInt, bottomPosData, 0, numBytesInInt);
            Array.Copy(data, 2 * numBytesInInt, leftPosData, 0, numBytesInInt);
            Array.Copy(data, 3 * numBytesInInt, rightPosData, 0, numBytesInInt);
            Array.Copy(data, 4 * numBytesInInt, widthData, 0, numBytesInInt);
            Array.Copy(data, 5 * numBytesInInt, heightData, 0, numBytesInInt);
            Array.Copy(data, 6 * numBytesInInt, imgData, 0, imgLength);

            using (MemoryStream ms = new MemoryStream(imgData, 0, imgData.Length))
            {
                ms.Write(imgData, 0, imgData.Length);
                bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = ms;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
            }

            int top = BitConverter.ToInt32(topPosData, 0);
            int bottom = BitConverter.ToInt32(bottomPosData, 0);
            int left = BitConverter.ToInt32(leftPosData, 0);
            int right = BitConverter.ToInt32(rightPosData, 0);

            int width = right - left + 1;
            int height = bottom - top + 1;

            resultion = new Win32Stuff.RESULTION
            {
                width = BitConverter.ToInt32(widthData, 0),
                height = BitConverter.ToInt32(heightData, 0)
            };
            bounds = new Int32Rect(left, top, width - 1, height - 1);
        }

        public static Bitmap GetCursorBitmap(ref int cursorX, ref int cursorY)
        {
            int screenWidth = 1;
            int screenHeight = 1;

            try
            {
                screenHeight = _newBitmap.Height;
                screenWidth = _newBitmap.Width;
            }
            catch (Exception)
            {

            }
            if (screenWidth == 1 && screenHeight == 1)
            {
                return null;
            }

            Bitmap img = ScreenCapture.CaptureCursor(ref cursorX, ref cursorY);

            if (img != null && cursorX < screenWidth && cursorY < screenHeight)
            {
                int width = img.Width;
                int height = img.Height;

                BitmapData imgData = img.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
                    img.PixelFormat);

                int stride = imgData.Stride;
                IntPtr scan0 = imgData.Scan0;

                unsafe
                {
                    byte* pByte = (byte*)(void*)scan0;

                    for (int h = 0; h < height; h++)
                    {
                        for (int w = 0; w < width; w++)
                        {
                            int offset = h * stride + w * numBytesPerPixel + 3;

                            if (*(pByte + offset) == 0)
                            {
                                *(pByte + offset) = 60;
                            }
                        }
                    }
                }
                img.UnlockBits(imgData);

                return img;
            }
            return null;
        }

        public static void UpdateScreenAndCursor(ref WriteableBitmap wbScreen, BitmapImage cursor, int cursorX, int cursorY)
        {


            wbScreen = wbScreen.Clone();

            Int32Rect rect = new Int32Rect(cursorX, cursorY, Convert.ToInt32(cursor.Width), Convert.ToInt32(cursor.Height));

            WriteableBitmap wbCursor = new WriteableBitmap(cursor);

            wbScreen.Blit(new Point(rect.X, rect.Y), wbCursor, new Rect(0, 0, rect.Width, rect.Height), Colors.White, WriteableBitmapExtensions.BlendMode.Alpha);

            wbScreen.Freeze();

        }

        public static void UpdateScreen(ref WriteableBitmap wbScreen, BitmapImage newScreen, Int32Rect bounds, byte[] imgData, Win32Stuff.RESULTION resultion)
        {
            Stopwatch test = new Stopwatch();
            test.Start();
            if (wbScreen == null)
            {
                wbScreen = new WriteableBitmap(resultion.width, resultion.height, 96, 96,
                    PixelFormats.Bgr32, null);
            }
            else
            {

                wbScreen = wbScreen.Clone();

            }


            WriteableBitmap wbNewScreen = new WriteableBitmap(newScreen);


            wbScreen.Blit(new Point(bounds.X, bounds.Y), wbNewScreen, new Rect(0, 0, bounds.Width, bounds.Height),
                 Colors.White, WriteableBitmapExtensions.BlendMode.Alpha);
            wbScreen.Freeze();
            test.Stop();

        }

        public static void UpdateScreen(ref Image screen, Image newPartialScreen, Rectangle boundingBox)
        {
            // Создаем первый экран, если он не существует.
            //
            if (screen == null)
            {
                screen = new Bitmap(boundingBox.Width, boundingBox.Height);
            }

            // Рисуем частичное изображение в текущем
            // screen. Это заменяет измененные пиксели.
            //
            Graphics g = null;
            try
            {
                lock (screen)
                {
                    g = Graphics.FromImage(screen);
                    g.DrawImage(newPartialScreen, boundingBox);
                    g.Flush();
                }
            }
            catch
            {
                // Do something with this info.
            }
            finally
            {
                if (g != null) g.Dispose();
            }
        }


        public static Image MergeScreenAndCursor(Image screen, Image cursor, int cursorX, int cursorY)
        {
            Image mergedImage = null;
            Graphics g = null;
            try
            {
                lock (screen)
                {
                    mergedImage = (Image)screen.Clone();
                }
                Rectangle r;
                lock (cursor)
                {
                    r = new Rectangle(cursorX, cursorY, cursor.Width, cursor.Height);
                }
                g = Graphics.FromImage(mergedImage);
                g.DrawImage(cursor, r);
                g.Flush();
            }
            catch (Exception)
            {
                // Do something with this info.
            }
            finally
            {
                if (g != null)
                {
                    g.Dispose();
                }
            }

            return mergedImage;
        }

        public static Image MergeScreenAndCursor(Image screen, Icon cursor, int cursorX, int cursorY)
        {
            Image mergedImage = (Image)screen.Clone();
            Rectangle r = new Rectangle(cursorX, cursorY, cursor.Width, cursor.Height);
            Graphics g = Graphics.FromImage(mergedImage);
            g.DrawIcon(cursor, r);
            g.Flush();
            g.Dispose();

            return mergedImage;
        }

    }
}
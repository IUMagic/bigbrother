﻿using System;

namespace BigBrother.Server.Library
{
    /// <summary>
    /// Класс для аргуменьа подключения/отключения пользователя, а также получения скриншота
    /// </summary>
    public class ConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// Массив объектов типа UserInfo
        /// </summary>
        public readonly UserInfo[] Ip;
        public readonly byte[] ImageBytes;
        public readonly bool Access;

        /// <summary>
        /// Конструктор для подключения/отключения пользователя
        /// </summary>
        /// <param name="ip">Ip клиента</param>
        public ConnectionEventArgs(UserInfo[] ip)
        {
            Ip = ip;
        }

        public ConnectionEventArgs(bool access)
        {
            Access = access;
        }

        /// <summary>
        /// Конструктор для получения изображения
        /// </summary>
        /// <param name="imagesBytes">Массив байтов</param>
        public ConnectionEventArgs(byte[] imagesBytes)
        {
            ImageBytes = imagesBytes;
        }
    }

}
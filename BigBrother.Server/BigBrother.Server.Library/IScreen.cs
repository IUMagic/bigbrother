﻿using System.ServiceModel;
using System.Windows.Forms;

namespace BigBrother.Server.Library
{
    /// <summary>
    /// Контракт службы
    /// </summary>
    [ServiceContract(CallbackContract = typeof(IScreenCallback), SessionMode = SessionMode.Required)]
    public interface IScreen
    {
        /// <summary>
        /// Операция обновлении положении курсора
        /// </summary>
        /// <param name="data">Координаты</param>
        [OperationContract]
        void updateCursor(string data);
        /// <summary>
        /// Операция нажатия левой кнопки
        /// </summary>
        [OperationContract]
        void MouseLeftUpClick();
        /// <summary>
        /// Операция отжатья левой кнопки
        /// </summary>
        [OperationContract]
        void MouseLeftDownClick();
        /// <summary>
        /// Операция нажатия правой кнопки
        /// </summary>
        [OperationContract]
        void MouseRightDownClick();
        /// <summary>
        /// Операция отжатия правой кнопки
        /// </summary>
        [OperationContract]
        void MouseRightUpClick();
        /// <summary>
        /// Операция скролла
        /// </summary>
        /// <param name="delta"></param>
        [OperationContract]
        void MouseWheell(int delta);
        /// <summary>
        /// Операция нажатия кнопки
        /// </summary>
        /// <param name="key">Название нажатой кнопки</param>
        /// <param name="vbyte">байт-код спец. кнопки Shift, Alt, Control, Win</param>
        [OperationContract]
        void KeyUp(string key, byte vbyte);
        /// <summary>
        /// Выключение
        /// </summary>
        [OperationContract]
        void ShutDown();
        /// <summary>
        /// Перезагрузка
        /// </summary>
        [OperationContract]
        void Restart();
        /// <summary>
        /// Выход из системы
        /// </summary>
        [OperationContract]
        void LogOff();
        /// <summary>
        /// Операция отправки сообщения
        /// </summary>
        /// <param name="message">Сообщение</param>
        [OperationContract]
        void SendMessage(string message);
        /// <summary>
        /// Операция подключения пользователя
        /// </summary>
        /// <param name="ip">Ip клиента</param>
        //[OperationContract]
        //void Connect(UserInfo ip);
        /// <summary>
        /// Операция получение разрешения на показ на сервере
        /// </summary>
        /// <param name="access">Разрешение на получения скриншота серверу</param>
        [OperationContract]
        void AccessForTranslation(bool access);
        /// <summary>
        /// Операция получения изображения с клиента
        /// </summary>
        /// <param name="imageBytes"></param>
        [OperationContract]
        void GetScreenShotForServer(byte[] imageBytes);
        /// <summary>
        /// Операция отключения пользователя
        /// </summary>
        /// <param name="ip">Ip клиента</param>
        //[OperationContract]
        //void Disconnect(UserInfo ip);
        /// <summary>
        /// Операция получения с сервера скриншота
        /// </summary>
        [OperationContract]
        void GetScreenShot(int indexPc, bool isOne);
        /// <summary>
        /// Операция получения имя пользователя сервера
        /// </summary>
        [OperationContract]
        void GetUserName();
    }
    
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace BigBrother.Server.Library
{
    public class BrowserCheck
    {
        public string[] ProccesList { get; } = new[] { "chrome", "fireofox", "opera", "iexplore" };

        public List<int> ProccessPid = new List<int>();

        public bool Checkproccess { get; set; } = false;

        public event EventHandler GetUrlBarEven = delegate { };

        public void CheckProcces()
        {
            while (true)
            {
                Checkproccess = false;
                foreach (var proccess in Process.GetProcesses())
                {
                    if (ProccesList.Contains(proccess.ProcessName))
                    {
                        if (!ProccessPid.Contains(proccess.Id))
                        {
                            ProccessPid.Add(proccess.Id);
                            Checkproccess = true;
                        }
                    }
                }
                if (Checkproccess) OnGetUrlBarEven();
                Thread.Sleep(1000);
            }
        }

        protected virtual void OnGetUrlBarEven()
        {
            GetUrlBarEven(this, EventArgs.Empty);
        }

    }
}

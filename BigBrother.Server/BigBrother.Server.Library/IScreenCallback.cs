﻿using System.ServiceModel;

namespace BigBrother.Server.Library
{
    /// <summary>
    /// Контракт обратного вызова
    /// </summary>
    [ServiceContract]
    public interface IScreenCallback
    {
        /// <summary>
        /// Операция оповещения о подключении нового пользователя
        /// </summary>
        /// <param name="ip">Ip</param>
        //[OperationContract(IsOneWay = true)]
        //void GetCoonectedPc(UserInfo[] ip);
        /// <summary>
        /// Операция оповещения о удачном получении скриншота
        /// </summary>
        /// <param name="response"></param>
        [OperationContract(IsOneWay = true)]
        void ResponseImageClient(string response);

        /// <summary>
        /// Операция отправления скриншота Клиенту
        /// </summary>
        /// <param name="image">Массив байта скриншота</param>
        /// <param name="indexPc">Номер компьютера в клиенте</param>
        [OperationContract(IsOneWay = true)]
        void ResponseImageServer(byte[] image, int indexPc, bool isOne);
        /// <summary>
        /// Операция отправления курсора Клиенту
        /// </summary>
        /// <param name="data">Массив байта курсора</param>
        [OperationContract(IsOneWay = true)]
        void ResponseCursorServer(byte[] data);
        /// <summary>
        /// Отправление имя подключенного пользователя Клиенту
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        [OperationContract(IsOneWay = true)]
        void ResponseUserName(string userName);


    }
}
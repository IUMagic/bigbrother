﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Point = System.Windows.Point;
using Color = System.Windows.Media.Color;
using Size = System.Windows.Size;

namespace BigBrother.Server.Library
{
    public static class WriteableBitmapExtensions
    {
        #region Enum

        /// <summary>
        ///   The blending mode.
        /// </summary>
        public enum BlendMode
        {
            /// <summary>
            ///   Alpha blendiing uses the alpha channel to combine the source and destination.
            /// </summary>
            Alpha,

            /// <summary>
            ///   Additive blending adds the colors of the source and the destination.
            /// </summary>
            Additive,

            /// <summary>
            ///   Subtractive blending subtracts the source color from the destination.
            /// </summary>
            Subtractive,

            /// <summary>
            ///   Uses the source color as a mask.
            /// </summary>
            Mask,

            /// <summary>
            ///   Multiplies the source color with the destination color.
            /// </summary>
            Multiply,

            /// <summary>
            ///   Ignores the specified Color
            /// </summary>
            ColorKeying,
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Copies (blits) the pixels from the WriteableBitmap source to the destination WriteableBitmap (this).
        /// </summary>
        /// <param name = "dest">The destination WriteableBitmap.</param>
        /// <param name = "destRect">The rectangle that defines the destination region.</param>
        /// <param name = "source">The source WriteableBitmap.</param>
        /// <param name = "sourceRect">The rectangle that will be copied from the source to the destination.</param>
        /// <param name = "blendMode">The blending mode <see cref = "BigBrother.Server.Library.WriteableBitmapExtensions.BlendMode" />.</param>
        public static void Blit(this WriteableBitmap dest, Rect destRect, WriteableBitmap source, Rect sourceRect,
                                 BlendMode blendMode)
        {
            Blit(dest, destRect, source, sourceRect, Colors.White, blendMode);
        }

        /// <summary>
        ///   Copies (blits) the pixels from the WriteableBitmap source to the destination WriteableBitmap (this).
        /// </summary>
        /// <param name = "dest">The destination WriteableBitmap.</param>
        /// <param name = "destRect">The rectangle that defines the destination region.</param>
        /// <param name = "source">The source WriteableBitmap.</param>
        /// <param name = "sourceRect">The rectangle that will be copied from the source to the destination.</param>
        public static void Blit(this WriteableBitmap dest, Rect destRect, WriteableBitmap source, Rect sourceRect)
        {
            Blit(dest, destRect, source, sourceRect, Colors.White, BlendMode.Alpha);
        }

        /// <summary>
        ///   Copies (blits) the pixels from the WriteableBitmap source to the destination WriteableBitmap (this).
        /// </summary>
        /// <param name = "dest">The destination WriteableBitmap.</param>
        /// <param name = "destPosition">The destination position in the destination bitmap.</param>
        /// <param name = "source">The source WriteableBitmap.</param>
        /// <param name = "sourceRect">The rectangle that will be copied from the source to the destination.</param>
        /// <param name = "color">If not Colors.White, will tint the source image. A partially transparent color and the image will be drawn partially transparent.</param>
        /// <param name = "blendMode">The blending mode <see cref = "BigBrother.Server.Library.WriteableBitmapExtensions.BlendMode" />.</param>
        public static void Blit(this WriteableBitmap dest, Point destPosition, WriteableBitmap source, Rect sourceRect,
                                 Color color, BlendMode blendMode)
        {
            Rect destRect = new Rect(destPosition, new Size(sourceRect.Width, sourceRect.Height));
            Blit(dest, destRect, source, sourceRect, color, blendMode);
        }

        /// <summary>
        ///   Copies (blits) the pixels from the WriteableBitmap source to the destination WriteableBitmap (this).
        /// </summary>
        /// <param name = "dest">The destination WriteableBitmap.</param>
        /// <param name = "destRect">The rectangle that defines the destination region.</param>
        /// <param name = "source">The source WriteableBitmap.</param>
        /// <param name = "sourceRect">The rectangle that will be copied from the source to the destination.</param>
        /// <param name = "color">If not Colors.White, will tint the source image. A partially transparent color and the image will be drawn partially transparent. If the BlendMode is ColorKeying, this color will be used as color key to mask all pixels with this value out.</param>
        /// <param name = "blendMode">The blending mode <see cref = "BigBrother.Server.Library.WriteableBitmapExtensions.BlendMode" />.</param>
        public static void Blit(this WriteableBitmap dest, Rect destRect, WriteableBitmap source, Rect sourceRect,
                                 Color color, BlendMode blendMode)
        {
            if (color.A == 0)
            {
                return;
            }

            int dpw = dest.PixelWidth;
            int dph = dest.PixelHeight;

            Rect intersect = new Rect(0, 0, dpw, dph);
            intersect.Intersect(destRect);
            if (intersect.IsEmpty)
            {
                return;
            }

            int sourceWidth = source.PixelWidth;

            source.Lock();
            dest.Lock();
            unsafe
            {
                var sourcePixels = (int*)source.BackBuffer;
                var destPixels = (int*)dest.BackBuffer;

                int sourceLength = source.BackBufferStride * source.PixelHeight;

                int sr = 0;
                int sg = 0;
                int sb = 0;
                int sa = 0;

                int ca = color.A;
                int cr = color.R;
                int cg = color.G;
                int cb = color.B;

                bool tinted = color != Colors.White;

                double sdx = sourceRect.Width / destRect.Width;
                double sdy = sourceRect.Height / destRect.Height;

                int sourceStartX = (int)sourceRect.X;
                int sourceStartY = (int)sourceRect.Y;

                const int lastii = -1;
                const int lastjj = -1;

                double jj = sourceStartY;

                int y = (int)destRect.Y;

                for (int j = 0; j < (int)destRect.Height; j++)
                {
                    if (y >= 0 && y < dph)
                    {
                        double ii = sourceStartX;
                        int idx = (int)destRect.X + y * dpw;
                        int x = (int)destRect.X;
                        int sourcePixel = sourcePixels[0];

                        for (int i = 0; i < (int)destRect.Width; i++)
                        {
                            if (x >= 0 && x < dpw)
                            {
                                if ((int)ii != lastii || (int)jj != lastjj)
                                {
                                    int sourceIdx = (int)ii + (int)jj * sourceWidth;
                                    if (sourceIdx >= 0 && sourceIdx < sourceLength)
                                    {
                                        sourcePixel = sourcePixels[sourceIdx];
                                        sa = ((sourcePixel >> 24) & 0xff);
                                        sr = ((sourcePixel >> 16) & 0xff);
                                        sg = ((sourcePixel >> 8) & 0xff);
                                        sb = ((sourcePixel) & 0xff);
                                        if (tinted && sa != 0)
                                        {
                                            sa = (((sa * ca) * 0x8081) >> 23);
                                            sr = ((((((sr * cr) * 0x8081) >> 23) * ca) * 0x8081) >> 23);
                                            sg = ((((((sg * cg) * 0x8081) >> 23) * ca) * 0x8081) >> 23);
                                            sb = ((((((sb * cb) * 0x8081) >> 23) * ca) * 0x8081) >> 23);
                                            sourcePixel = (sa << 24) | (sr << 16) | (sg << 8) | sb;
                                        }
                                    }
                                    else
                                    {
                                        sa = 0;
                                    }
                                }

                                if (blendMode == BlendMode.ColorKeying)
                                {
                                    sr = ((sourcePixel >> 16) & 0xff);
                                    sg = ((sourcePixel >> 8) & 0xff);
                                    sb = ((sourcePixel) & 0xff);

                                    if (sr != color.R || sg != color.G || sb != color.B)
                                    {
                                        destPixels[idx] = sourcePixel;
                                    }
                                }
                                else
                                {
                                    int dr;
                                    int db;
                                    int da;
                                    int dg;
                                    if (blendMode == BlendMode.Mask)
                                    {
                                        int destPixel = destPixels[idx];
                                        da = ((destPixel >> 24) & 0xff);
                                        dr = ((destPixel >> 16) & 0xff);
                                        dg = ((destPixel >> 8) & 0xff);
                                        db = ((destPixel) & 0xff);
                                        destPixel = ((((da * sa) * 0x8081) >> 23) << 24) |
                                                    ((((dr * sa) * 0x8081) >> 23) << 16) |
                                                    ((((dg * sa) * 0x8081) >> 23) << 8) |
                                                    ((((db * sa) * 0x8081) >> 23));
                                        destPixels[idx] = destPixel;
                                    }
                                    else if (sa > 0)
                                    {
                                        int destPixel = destPixels[idx];
                                        da = ((destPixel >> 24) & 0xff);
                                        if ((sa == 255 || da == 0) &&
                                             blendMode != BlendMode.Additive
                                             && blendMode != BlendMode.Subtractive
                                             && blendMode != BlendMode.Multiply
                                            )
                                        {
                                            destPixels[idx] = sourcePixel;
                                        }
                                        else
                                        {
                                            dr = ((destPixel >> 16) & 0xff);
                                            dg = ((destPixel >> 8) & 0xff);
                                            db = ((destPixel) & 0xff);
                                            if (blendMode == BlendMode.Alpha)
                                            {
                                                destPixel = ((sa + (((da * (255 - sa)) * 0x8081) >> 23)) << 24) |
                                                            ((sr + (((dr * (255 - sa)) * 0x8081) >> 23)) << 16) |
                                                            ((sg + (((dg * (255 - sa)) * 0x8081) >> 23)) << 8) |
                                                            ((sb + (((db * (255 - sa)) * 0x8081) >> 23)));
                                            }
                                            else if (blendMode == BlendMode.Additive)
                                            {
                                                int a = (255 <= sa + da) ? 255 : (sa + da);
                                                destPixel = (a << 24) |
                                                            (((a <= sr + dr) ? a : (sr + dr)) << 16) |
                                                            (((a <= sg + dg) ? a : (sg + dg)) << 8) |
                                                            (((a <= sb + db) ? a : (sb + db)));
                                            }
                                            else if (blendMode == BlendMode.Subtractive)
                                            {
                                                int a = da;
                                                destPixel = (a << 24) |
                                                            (((sr >= dr) ? 0 : (sr - dr)) << 16) |
                                                            (((sg >= dg) ? 0 : (sg - dg)) << 8) |
                                                            (((sb >= db) ? 0 : (sb - db)));
                                            }
                                            else if (blendMode == BlendMode.Multiply)
                                            {
                                                // Faster than a division like (s * d) / 255 are 2 shifts and 2 adds
                                                int ta = (sa * da) + 128;
                                                int tr = (sr * dr) + 128;
                                                int tg = (sg * dg) + 128;
                                                int tb = (sb * db) + 128;

                                                int ba = ((ta >> 8) + ta) >> 8;
                                                int br = ((tr >> 8) + tr) >> 8;
                                                int bg = ((tg >> 8) + tg) >> 8;
                                                int bb = ((tb >> 8) + tb) >> 8;

                                                destPixel = (ba << 24) |
                                                            ((ba <= br ? ba : br) << 16) |
                                                            ((ba <= bg ? ba : bg) << 8) |
                                                            ((ba <= bb ? ba : bb));
                                            }

                                            destPixels[idx] = destPixel;
                                        }
                                    }
                                }
                            }
                            x++;
                            idx++;
                            ii += sdx;
                        }
                    }
                    jj += sdy;
                    y++;
                }
            }
            dest.AddDirtyRect(new Int32Rect(0, 0, dest.PixelWidth, dest.PixelHeight));
            dest.Unlock();
            source.Unlock();
        }

        #endregion
    }
}
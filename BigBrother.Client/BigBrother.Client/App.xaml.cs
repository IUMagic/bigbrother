﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using BigBrother.Client.View;
using BigBrother.Client.ViewModel;

namespace BigBrother.Client
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static DisplayRootRegistry displayRootRegistry = new DisplayRootRegistry();
        public static LogonViewModel LogonVM;
        public static ActivitiesusersViewModel ActivitiesusersViewModel;
        public static MainViewModel MainViewModel;
        public static OneWindowViewModel OneWindowViewModel;
        public App()
        {
            displayRootRegistry.RegisterWindowType<MainViewModel, MainWindow>();
            displayRootRegistry.RegisterWindowType<LogonViewModel, LogonWindow>();
            displayRootRegistry.RegisterWindowType<ActivitiesusersViewModel, ActivitiesusersWindow>();
            displayRootRegistry.RegisterWindowType<OneWindowViewModel, OneWindow>();
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            await RunProgramLogic();
        }


        async Task RunProgramLogic()
        {

            //LogonVM = new LogonViewModel();
            //ActivitiesusersViewModel = new ActivitiesusersViewModel();
            MainViewModel = new MainViewModel();
            displayRootRegistry.ShowPresentation(MainViewModel);  

        }
    }
}

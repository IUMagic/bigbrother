﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace BigBrother.Client.Model
{
    public class PeopleModel : ValidationRule, INotifyPropertyChanged
    {

        private string _login;

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged("Login");
            }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged("Password");
            }
        }


        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int res;
            bool isInt = Int32.TryParse(value.ToString(), out res);
            if (isInt)
            {
                return new ValidationResult(false, "Вы являетесь студентом");
            } else { 
                return new ValidationResult(true,null);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

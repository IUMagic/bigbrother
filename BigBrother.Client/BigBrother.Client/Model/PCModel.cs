﻿using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace BigBrother.Client.Model
{
    public class PcModel : INotifyPropertyChanged
    {
        public string NamePC { get; set; }
        public string IpPC { get; set; }
        public int indexPc { get; set; }

        private bool _isAccessTranstion;

        public bool IsAccessTranslation
        {
            get { return _isAccessTranstion; }
            set
            {
                _isAccessTranstion = value;
                OnPropertyChanged("IsAccessTranslation");
            }
        }

        private BitmapSource _image;

        public BitmapSource Image
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged("Image");
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }

    public class ImageOne : INotifyPropertyChanged
    {
       

        private BitmapSource _image;

        public BitmapSource Image
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged("Image");
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
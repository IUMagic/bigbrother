﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using BigBrother.Client.Annotations;

namespace BigBrother.Client.Model
{
    public class AccessModel : INotifyPropertyChanged
    {
        private bool _isAccessTranslation;
        public bool IsAccessTranslation
        {
            get { return _isAccessTranslation; }
            set
            {
                _isAccessTranslation = value;
                OnPropertyChanged("IsAccessTranslation");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
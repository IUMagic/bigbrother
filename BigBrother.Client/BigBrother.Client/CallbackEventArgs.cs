﻿using System;
using System.Drawing;
using System.Xaml;
using BigBrother.Client.ServiceReference1;

namespace BigBrother.Client
{
    public class CallbackEventArgs : EventArgs
    {
        //public readonly UserInfo[] Ip;
        public readonly byte[] ImageBytes;
        public readonly int IndexPc;
        public readonly bool IsOne;

       /* public CallbackEventArgs(UserInfo[] ip)
        {
            Ip = ip;
        }*/

        public CallbackEventArgs(byte[] imageBytes, int indexPc, bool isOne)
        {
            ImageBytes = imageBytes;
            IndexPc = indexPc;
            IsOne = isOne;
        }

        public CallbackEventArgs(byte[] data)
        {
            ImageBytes = data;
        }

    }
}

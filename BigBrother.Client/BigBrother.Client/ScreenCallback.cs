﻿using System;
using System.ServiceModel;
using BigBrother.Client.ServiceReference1;
namespace BigBrother.Client
{
    [CallbackBehavior(UseSynchronizationContext = false, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class ScreenCallback : IScreenCallback
    {
        //public event EventHandler<CallbackEventArgs> UsersEven = delegate { };
        public event EventHandler<CallbackEventArgs> ImageGetEven = delegate { };
        public event EventHandler<CallbackEventArgs> ImageGetEvenOne = delegate { };
        public event EventHandler<CallbackEventArgs> CursorGetEvenOne = delegate { };
       /* public void GetCoonectedPc(UserInfo[] ip)
        {
            CallbackEventArgs args = new CallbackEventArgs(ip);
            OnUsersEvent(args);
        }*/

        public void ResponseImageClient(string response)
        {
            Console.WriteLine(response);
            GC.Collect();
        }

        public void ResponseCursorServer(byte[] data)
        {
            CallbackEventArgs args = new CallbackEventArgs(data);
            OnCursorGetEvenOne(args);

        }

        public void ResponseImageServer(byte[] image, int indexPc, bool isOne)
        {
            CallbackEventArgs args = new CallbackEventArgs(image, indexPc, isOne);
            Console.WriteLine($"Доступ: {isOne}");
            if (isOne)
            {
                OnImageGetEven(args);
            }
            else
            {
                OnImageGetEvenOne(args);
            }
        }

        public void ResponseUserName(string userName)
        {
            Console.WriteLine(userName);
        }


        /*protected virtual void OnUsersEvent(CallbackEventArgs e)
        {
            if (UsersEven != null)
            {
                UsersEven(this, e);
            }
        }*/

        protected virtual void OnImageGetEven(CallbackEventArgs e)
        {
            if (ImageGetEven != null)
                ImageGetEven(this, e);
        }

        protected virtual void OnCursorGetEvenOne(CallbackEventArgs e)
        {
            if (CursorGetEvenOne != null)
                CursorGetEvenOne(this, e);
        }

        protected virtual void OnImageGetEvenOne(CallbackEventArgs e)
        {
            if (ImageGetEvenOne != null)
                ImageGetEvenOne(this, e);
        }

        
    }
}
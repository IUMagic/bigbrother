﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBrother.Client
{
    class ActiveDirectory
    {
            #region Переменные

        readonly string sDomain = "iat.iat";
        readonly  string sDefaultOU = "OU=test,DC=domain,DC=local";
        readonly string sDefaultRootOU = "DC=iat,DC=iat";
        protected  string sServiceUser;
        protected  string sServicePassword;

        public ActiveDirectory(string username, string password)
        {
            sServiceUser = username;
            sServicePassword = password;
        }

        #endregion


        /// <summary>
        /// Проверка имени пользователя и пароля
        /// </summary>
        /// <param name="sUserName">Имя пользователя</param>
        /// <param name="sPassword">Пароль</param>
        /// <returns>Возвращает true, если имя и пароль верны</returns>
        public bool ValidateCredentials(string sUserName, string sPassword)
        {
            return GetPrincipalContext().ValidateCredentials(sUserName, sPassword);
        }

        /// <summary>
        /// Получить указанного пользователя Active Directory
        /// </summary>
        /// <param name="sUserName">Имя пользователя для извлечения</param>
        /// <returns>Объект UserPrincipal</returns>
        public  UserPrincipal GetUser(string sUserName)
        {
            PrincipalContext oPrincipalContext = GetPrincipalContext();
            return UserPrincipal.FindByIdentity(oPrincipalContext, IdentityType.SamAccountName, sUserName);
        }


        /// <summary>
        /// Возвращает список групп, в которых состоит пользователь
        /// </summary>
        /// <param name="sUserName">Имя пользователя</param>
        /// <returns>Возвращает List со всеми группами пользователя</returns>
        public List<string> GetUserGroups(string sUserName)
        {
            List<string> myItems = new List<string>();
            UserPrincipal oUserPrincipal = GetUser(sUserName);

            using (PrincipalSearchResult<Principal> oPrincipalSearchResult = oUserPrincipal.GetGroups())
            {
                foreach (Principal oResult in oPrincipalSearchResult)
                {
                    myItems.Add(oResult.Name);
                }
            }

            return myItems;
        }



        /// <summary>
        /// Получить базовый основной контекст
        /// </summary>
        /// <returns>Возвращает объект PrincipalContext</returns>
        public PrincipalContext GetPrincipalContext()
        {
            return new PrincipalContext(ContextType.Domain, sDomain, sDefaultRootOU, sServiceUser, sServicePassword);
        }


    }
}

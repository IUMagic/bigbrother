﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BigBrother.Client.ViewModel;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;

namespace BigBrother.Client.View
{
    /// <summary>
    /// Логика взаимодействия для OneWindow.xaml
    /// </summary>
    public partial class OneWindow : Window
    {
        private ModifierKeys oldKeys;      
        public OneWindow()
        {
            InitializeComponent();
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            OneWindowViewModel.SendCursor(sender, e, test);
        }

        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            OneWindowViewModel.SendMouseWhell(e.Delta);
        }
      

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            string key = "";
            byte vbyte = 0x00;
            ModifierKeys Modifers = e.KeyboardDevice.Modifiers;
            string[] ModifersArray = e.KeyboardDevice.Modifiers.ToString().Split(',');

            switch (e.KeyboardDevice.Modifiers)
                {
                    case ModifierKeys.Alt :
                        vbyte = 0x12;
                        break;
                    case ModifierKeys.Shift:
                        vbyte = e.Key == Key.LeftShift ? (byte) 0xA0 : (byte) 0xA1;
                        break;
                    case ModifierKeys.Control:
                        vbyte = e.Key == Key.LeftCtrl ? (byte) 0xA2 : (byte) 0xA3;
                        break;
                    case ModifierKeys.Windows:
                        vbyte = e.Key == Key.LWin ? (byte) 0x5B : (byte) 0x5C;
                        break;

                }
            Console.WriteLine(e.KeyboardDevice.Modifiers);
         
                if ((e.KeyboardDevice.Modifiers) == (ModifierKeys.Alt | ModifierKeys.Shift))
                {
                    vbyte = 0xA0;
                }
            

            /*  if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
              {
                  key = e.Key.ToString();
              }
              else
              {
                  key = e.Key.ToString().ToLower();
  
              }*/
            /* if (InputLanguage.CurrentInputLanguage.Culture.IetfLanguageTag.Equals("ru-RU"))
             {
                 int i = Array.IndexOf(eng, key[0]);
                 key = key.Replace(key[0], rus[i]);
             }*/
          
                OneWindowViewModel.SendKeys(e.Key.ToString().ToLower(), vbyte);
          
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigBrother.Server.Library.DataModel;

namespace BigBrother.Client.ViewModel
{
    public class ActivitiesusersViewModel
    {
        public ActivitiesusersViewModel()
        {
            using (LibraryContext context = new LibraryContext())
            {
                var result = from activitiesusers in context.ActivitiesUsers
                                   join users in context.Users on activitiesusers.UserId equals users.UserId
                                   join machine in context.Pcs on activitiesusers.PcId equals machine.PcId
                                   select new
                                   {
                                       Id = activitiesusers.ActivitiesUserId,
                                       Url = activitiesusers.Url,
                                       DateTime = activitiesusers.DateTime,
                                       IsBlock = activitiesusers.IsBlock,
                                       User = users.Login,
                                       PC=  machine.PcName
                                   };
                foreach (var VARIABLE in result)
                {
                   _activitiesUsers.Add(new ActivityTable()
                   {
                       Url = VARIABLE.Url,
                       DateTime = VARIABLE.DateTime.ToString("f"),
                       IsBlock = VARIABLE.IsBlock,
                       User = VARIABLE.User,
                       Pc = VARIABLE.PC
                   });
                }
            }
        }
        private ObservableCollection<ActivityTable> _activitiesUsers = new ObservableCollection<ActivityTable>();
        public ObservableCollection<ActivityTable> ActivitiesUser
        {
            get { return _activitiesUsers; }
            set { _activitiesUsers = value; }
        }
    }

    public class ActivityTable
    {
        public string Url { get; set; }
        public string DateTime { get; set; }
        public bool IsBlock { get; set; }
        public string User { get; set; }
        public string Pc { get; set; }
    }
}

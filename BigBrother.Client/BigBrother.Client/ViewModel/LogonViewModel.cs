﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using BigBrother.Client.Model;

namespace BigBrother.Client.ViewModel
{
    public class PasswordBehavior : Behavior<PasswordBox>
    {
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(PasswordBehavior), new PropertyMetadata(default(string)));

        private bool _skipUpdate;

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.PasswordChanged += PasswordBox_PasswordChanged;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PasswordChanged -= PasswordBox_PasswordChanged;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == PasswordProperty)
            {
                if (!_skipUpdate)
                {
                    _skipUpdate = true;
                    AssociatedObject.Password = e.NewValue as string;
                    _skipUpdate = false;
                }
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            _skipUpdate = true;
            Password = AssociatedObject.Password;
            _skipUpdate = false;
        }
    }

    public class LogonViewModel
    {

        MainViewModel mainViewModel;
        public LogonViewModel()
        {
            ClickCommand = new Command(arg => Logon());
            People = new PeopleModel();
        }

        private async void Logon()
        {
            
            ActiveDirectory activeDirectory = new ActiveDirectory(People.Login, People.Password);
            try
            {
                if (activeDirectory.ValidateCredentials(People.Login, People.Password))
                {
                    List<string> getGroupList = activeDirectory.GetUserGroups(People.Login);
                    foreach (var group in getGroupList)
                    {
                        if (group.Equals("students"))
                        {
                            throw new Exception("Вы является студентом");
                        }
                        else
                        {
                            mainViewModel = new MainViewModel();
                            App.displayRootRegistry.ShowModalPresentation(mainViewModel);
                            App.displayRootRegistry.HidePresentation(App.LogonVM);
                            break;
                        }
                    }
                }
                else
                {
                    throw new Exception("Логин или пароль введен не верно");
                }

            }
            catch(Exception ex)
            {
               Console.WriteLine(ex.Message); 
            }
            
            

        }
        public PeopleModel People { get; set; }
        public ICommand ClickCommand { get; set; }
    }
}

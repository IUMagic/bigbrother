﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Proxies;
using System.ServiceModel;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using BigBrother.Client.Model;
using BigBrother.Server.Library;

namespace BigBrother.Client.ViewModel
{
    public class OneWindowViewModel
    {
        private static int indexPc;
        private static WriteableBitmap oldImage = null;
        static Timer _t = new Timer(); //Промежуточный Timer
        private static BitmapImage cursorTest;
        private static int cursorXTest = 0;
        private static int cursorYTest = 0;

        public OneWindowViewModel(int indexpc)
        {
            _t = new Timer();
            indexPc = indexpc;
            MouseLeftUpClickCommand = new Command(arg => MouseLeftUpClick());
            MouseLeftDownClickCommand = new Command(arg => MouseLeftDownClick());
            MouseRightUpClickCommand = new Command(arg => MouseRightUpClick());
            MouseRightDownClickCommand = new Command(arg => MouseRightDownClick());
            CloseCommand = new Command(arg => Close());
            PcModel = new PcModel
            {
                Image = null
            };



            _t.Interval = 100;
            _t.Elapsed += GetScreenshot;
            _t.Start();
        }

        public static void timerOff()
        {
            _t.Stop();
            _t.Close();
            _t.Dispose();

        }

    public static PcModel PcModel { get; set; }

        public OneWindowViewModel()
        {

        }


        public void GetScreenshot(object sender, ElapsedEventArgs e)
        {
            
            if (!MainViewModel.isVideo)
                try
                {
                    MainViewModel.proxy[indexPc].GetScreenShot(indexPc, MainViewModel.isVideo);
                }
                catch (CommunicationException)
                {
                     
                }
        }
        /// <summary>
        /// Отпуск левой кнопки
        /// </summary>
        public ICommand MouseLeftUpClickCommand { get; set; }
        /// <summary>
        /// Нажатие левой кнопки
        /// </summary>
        public ICommand MouseLeftDownClickCommand { get; set; }
        /// <summary>
        /// Отпуск правой кнопки
        /// </summary>
        public ICommand MouseRightUpClickCommand { get; set; }
        /// <summary>
        /// Нажатие правой кнопки
        /// </summary>
        public ICommand MouseRightDownClickCommand { get; set; }
        /// <summary>
        /// Закрытие
        /// </summary>
        public ICommand CloseCommand { get; set; }

        private void MouseLeftUpClick()
        {
            try
            {
                MainViewModel.proxy[indexPc].MouseLeftUpClick();
            }
            catch (CommunicationException)
            {
               
            } 
        }

        private void MouseLeftDownClick()
        {
            try
            {
                MainViewModel.proxy[indexPc].MouseLeftDownClick();
            }
            catch (CommunicationException)
            {
               
            } 
        }

        private void MouseRightDownClick()
        {
            try
            {
                MainViewModel.proxy[indexPc].MouseRightDownClick();
            }
            catch (CommunicationException)
            {
               
            } 
        }

        private void MouseRightUpClick()
        {
            try
            {
                MainViewModel.proxy[indexPc].MouseRightUpClick();
            }
            catch (CommunicationException)
            {
               
            } 
        }

        public static void SendKeys(string keys, byte vbyte)
        {
            try
            {
                MainViewModel.proxy[indexPc].KeyUp(keys, vbyte);
            }
            catch (CommunicationException)
            {

            }
        }

        private void Close()
        {
            MainViewModel.isVideo = true;
            OneWindowViewModel.timerOff();
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static void callback_GetCursorEven(object sender, CallbackEventArgs e)
        {
            BitmapImage cursor;
            int cursorX, cursorY;
            if (e.ImageBytes != null)
            {
                Function.UnpackCursor(e.ImageBytes, out cursor, out cursorX, out cursorY);
                cursorTest = cursor;
                cursorXTest = cursorX;
                cursorYTest = cursorY;
            }
        }

        public static void SendMouseWhell(int delta)
        {
            try
            {
                MainViewModel.proxy[OneWindowViewModel.indexPc].MouseWheell(delta);
            }
            catch (CommunicationException)
            {

            }
        }
        /// <summary>
        /// Формируем и отправляем координаты в соответсвии разрешению источника
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="test"></param>
        public static void SendCursor(object sender, MouseEventArgs e, Image test)
        {
            if (test.Source != null)
            {
                var relativePosition = e.GetPosition(sender as IInputElement);
                int cursorX = Convert.ToInt32(relativePosition.X * test.Source.Width / test.ActualWidth);
                int cursorY = Convert.ToInt32(relativePosition.Y * test.Source.Height / test.ActualHeight);
                string data = cursorX + "," + cursorY;
                try
                {
                    MainViewModel.proxy[OneWindowViewModel.indexPc].updateCursor(data);
                }
                catch (CommunicationException)
                {
                 
                }
               
            }
        }

        public static void callback_GetImageEven(object sender, CallbackEventArgs e)
        {


            BitmapImage partial;
            Int32Rect bounds;
            Win32Stuff.RESULTION resultion;
            byte[] imgData;
            if (e.ImageBytes != null)
            {

                Function.UnpackImage(e.ImageBytes, out partial, out bounds, out imgData, out resultion);
                //WriteableBitmap test = oldImage[e.IndexPc];
               Function.UpdateScreen(ref oldImage, partial, bounds, imgData, resultion);
                if (cursorTest != null)
                {
                    Function.UpdateScreenAndCursor(ref oldImage, cursorTest, cursorXTest, cursorYTest);
                }
                // oldImage[e.IndexPc] = test;
                // WriteableBitmap test = oldImage;
                // test.Freeze();
                PcModel.Image = oldImage;
                GC.Collect();
            }
        }
    }
}
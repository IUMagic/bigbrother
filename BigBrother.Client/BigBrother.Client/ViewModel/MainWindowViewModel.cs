﻿
using System.ServiceModel;
using System.Windows.Input;
using BigBrother.Client.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.ServiceModel.Discovery;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using BigBrother.Client.ServiceReference1;
using BigBrother.Server.Library;
using WpfAnimatedGif;
using Image = System.Drawing.Image;
using IScreen = BigBrother.Client.ServiceReference1.IScreen;

namespace BigBrother.Client.ViewModel
{
    /*Добавление к классу System.Timers.Timer - IndexPc, Ip.
    // Для дополнительной информции
    */
    class Timer : System.Timers.Timer
    {
        public int IndexPc { get; set; }

        public string Ip { get; set; }

        public String Address { get; set; }
    }




    public class MainViewModel
    {  
        //public List<IScreenCallback> proxy = new List<IScreenCallback>(); //Адреса Server
        /// <summary>
        /// Лист в котором содержаться данные для связи с источником
        /// </summary>
        public static List<ScreenClient> proxy = new List<ScreenClient>();
        /// <summary>
        /// Указывает на живые источники
        /// </summary>
        Hashtable liveService = new Hashtable();
        /// <summary>
        /// Старые изображения источников
        /// </summary>
        private List<WriteableBitmap> oldImage = new List<WriteableBitmap>();
        // UserInfo user = null;
        /// <summary>
        /// Таймер для получения изображения с серверов
        /// </summary>
        private readonly List<Timer> _timerGetImage = new List<Timer>(); 
        Timer _t = new Timer();
        ActivitiesusersViewModel ActivitiesusersViewModel;
        private OneWindowViewModel OneWindowViewModel;
        /// <summary>
        /// Открыто ли окно для управления и просмотора отдельного источника
        /// </summary>
        public static bool isVideo = true;
        Rectangle bounds = Rectangle.Empty;
        const int numBytesPerPixel = 4;
        private Win32Stuff.RESULTION resoulution;
        private Bitmap _prevImage;
        private Bitmap _newBitmap = new Bitmap(1, 1);
        public MainViewModel()
        {
            
 

            GetPc.Add(new PcModel()
            {
                IpPC = "l204-iumag",
                NamePC = "l204-iumag",
                indexPc = 0
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l204-bik",
                NamePC = "l204-bik",
                indexPc = 1
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l204-ws01",
                NamePC = "l204-ws01",
                indexPc = 2
            });
            /*
            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws03",
                NamePC = "l228-ws03",
                indexPc = 2
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws04",
                NamePC = "l228-ws04",
                indexPc = 3
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws05",
                NamePC = "l228-ws05",
                indexPc = 4
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws06",
                NamePC = "l228-ws06",
                indexPc = 5
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws07",
                NamePC = "l228-ws07",
                indexPc = 6
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws08",
                NamePC = "l228-ws08",
                indexPc = 7
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws09",
                NamePC = "l228-ws09",
                indexPc = 8
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws10",
                NamePC = "l228-ws10",
                indexPc = 9
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws11",
                NamePC = "l228-ws11",
                indexPc = 10
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws12",
                NamePC = "l228-ws12",
                indexPc = 11
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws13",
                NamePC = "l228-ws13",
                indexPc = 12
            });

            GetPc.Add(new PcModel()
            {
                IpPC = "l228-ws14",
                NamePC = "l228-ws14",
                indexPc = 13
            });



         */
            Access = new AccessModel
                 {
                     IsAccessTranslation = false
                 };

            
            ClickCommand = new Command(arg => ClickMethod());
            OpenSiteCommand = new Command(arg => OpenSite());
            OpenWindowCommand = new Command(arg => OpenWindow(arg as PcModel), (arg) =>
            {
                PcModel pcModel = arg as PcModel;
                if (liveService[pcModel.IpPC].Equals(true)) return true;
                System.Windows.MessageBox.Show($"Компьюьтер {pcModel.NamePC.ToUpper()} не доступен. Дождитесь загрузки или обратитесь к администратору!");
                return false;
            });
            CloseAppCommand = new Command(arg => CloseApp());
            ToDoCommand = new Command(arg=>ToDo());
            SendMessageCommand = new Command(arg=>SendMessage());
            ShutDownCommand = new Command(arg=>ShutDown());
            RestartCommand = new Command(arg=>Restart());
            LogOffCommand = new Command(arg=>LogOff());
            foreach (var pc in GetPc)
            {
                liveService.Add(pc.IpPC, false);
                proxy.Add(null);
                oldImage.Add(null);
                _timerGetImage.Add(null);
                GetPc[pc.indexPc].Image = new BitmapImage(new Uri("pack://application:,,,/Resources/noconnect.png"));
                
            }
            Timer _t = new Timer();
            _t.Interval = 5000;
            _t.Elapsed += GetLive;
            _t.Start();
        }

        public AccessModel Access { get; set; }
        public ObservableCollection<PcModel> GetPc { get; } = new ObservableCollection<PcModel>();

        private PcModel _CurrentPcModel;
        public PcModel CurrentPcModel
        {
            get { return _CurrentPcModel; }
            set
            {
                if (_CurrentPcModel == value) return;
                _CurrentPcModel = value;
                OnPropertyChanged("CurrentPcModel");
            }
        }
        private void OpenSite()
        {
            ActivitiesusersViewModel = new ActivitiesusersViewModel();
            App.displayRootRegistry.ShowModalPresentation(ActivitiesusersViewModel);
        }

        private void CloseApp()
        {

            Environment.Exit(1);
        }

        private void ToDo()
        {
            System.Windows.MessageBox.Show("ToDo");
        }

        private void SendMessage()
        {
            try
            {
                proxy[CurrentPcModel.indexPc].SendMessage("test");
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Не доступен");
            } 
        }

        private void ShutDown()
        {
            try
            {
                proxy[CurrentPcModel.indexPc].ShutDown();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Не доступен");
            }
        }

        private void Restart()
        {
            try
            {
                proxy[CurrentPcModel.indexPc].Restart();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Не доступен");
            }
        }

        private void LogOff()
        {
            try
            {
                proxy[CurrentPcModel.indexPc].LogOff();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Не доступен");
            }
        }

        private void OpenWindow(PcModel arg)
        {
            isVideo = false;
            OneWindowViewModel = new OneWindowViewModel(Convert.ToInt32(arg.indexPc));
            App.displayRootRegistry.ShowModalPresentation(OneWindowViewModel);
        }
        private void ClickMethod()
        {
            Access.IsAccessTranslation = !Access.IsAccessTranslation;
            //Поиск источников от которых идет сигнал
            var screenClients = proxy.Where(x=>x != null).Where(x=>x.State == CommunicationState.Opened);
            foreach (var screenClient in screenClients.ToArray())
            {
                try
                {
                    screenClient.AccessForTranslation(Access.IsAccessTranslation);
                }
                catch(CommunicationException)
                {
                    
                }
            }
            
            
            if (Access.IsAccessTranslation == true)
            {
                _t = new Timer(); //Промежуточный Timer
                _t.Interval = 500;
                _t.Elapsed += AccessForTranslation;
                _t.Start();
            }
            else
            {
                _t.Stop();
                _t.Close();
                _t.Dispose();
                GC.Collect();
            }
        }

        void AccessForTranslation(object sender, ElapsedEventArgs e)
        {
            //Поиск источников от которых идет сигнал
            var screenClients = proxy.Where(x => x != null).Where(x => x.State == CommunicationState.Opened);
            int cursorX = 0;
            int cursorY = 0;
            byte[] data = null;
            Bitmap b = Function.GetScreenBitmap(ref bounds, ref resoulution);
            if (b != null && bounds != Rectangle.Empty)
            {
                data = Function.PackImage(b, bounds, resoulution);
            }
            foreach (ScreenClient screenClient in screenClients.ToArray())
            {
               
                try
                {
                    screenClient.GetScreenShotForServer(data);
                }
                catch (CommunicationException)
                {
                    ((Timer) sender).Stop();
                    ((Timer)sender).Dispose();
                }
            }
        }

        /// <summary>
        /// Поиск включенных источников, которые соответсвуют компьютеров, заданные в настройках
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void GetLive(object sender, ElapsedEventArgs e)
        {
            string[] getLivEndpointAddress = GetAvailableAddresses();
            if (getLivEndpointAddress == null) return;
            foreach (var LiveEndpointAddress in getLivEndpointAddress)
            {
                if (liveService.Contains(LiveEndpointAddress))
                    if (!liveService[LiveEndpointAddress].Equals(true))
                    {
                        liveService[LiveEndpointAddress] = true;
                        IEnumerable<PcModel> test = GetPc.Where(x => x.IpPC == LiveEndpointAddress);
                        foreach (var name in test)
                        {
                            StartScreen(LiveEndpointAddress, GetPc.IndexOf(name));
                        }
                        Console.WriteLine(liveService[LiveEndpointAddress]);
                    }
            }
        }

        /// <summary>
        /// Подключение к источнику
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="indexPc"></param>
        void StartScreen(string ip, int indexPc)
        {
            GetPc[indexPc].Image = null;
            ScreenCallback callback = new ScreenCallback();   
            callback.ImageGetEven += new EventHandler<CallbackEventArgs>(callback_GetImageEven);
            callback.ImageGetEvenOne += new EventHandler<CallbackEventArgs>(OneWindowViewModel.callback_GetImageEven);
            callback.CursorGetEvenOne += new EventHandler<CallbackEventArgs>(OneWindowViewModel.callback_GetCursorEven);
            InstanceContext ctx = new InstanceContext(callback);
            NetTcpBinding netTcpBinding = new NetTcpBinding();
            netTcpBinding.Security.Mode = SecurityMode.None;
            EndpointAddress address = new EndpointAddress("net.tcp://" + ip + ":8090/ScreenService");
            ScreenClient screenClient = new ScreenClient(ctx, "NetTcpBinding_IScreen", address);
            proxy[indexPc] = screenClient;
            try
            {
                proxy[indexPc].GetUserName();
                proxy[indexPc].AccessForTranslation(Access.IsAccessTranslation);
            }
            catch (Exception)
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action) (() =>
                {
                    GetPc[indexPc].Image = new BitmapImage(new Uri("pack://application:,,,/Resources/noconnect.png"));
                }));
                proxy[indexPc] = null;
                liveService[ip] = false;
            }
          
            Timer _t = new Timer(); //Промежуточный Timer

            _t.Interval = 500;
            _t.IndexPc = indexPc;
            _t.Ip = ip;
            _t.Elapsed += GetImagePc;
            _t.Start();
            _timerGetImage[indexPc] = _t;
        }

        private void GetImagePc(object sender, ElapsedEventArgs e)
        {
        
            if (!Access.IsAccessTranslation && isVideo)
            {
                int indexPc = Convert.ToInt32(((Timer) sender).IndexPc);
                string ip = Convert.ToString(((Timer) sender).Ip);
                if (Access.IsAccessTranslation) return;
                try
                {
                   proxy[indexPc].GetScreenShot(indexPc, isVideo);
                }
                catch (Exception)
                {

                    liveService[ip] = false;

                    System.Windows.Application.Current.Dispatcher.Invoke((Action) (() =>
                    {
                        GetPc[indexPc].Image = new BitmapImage(new Uri("pack://application:,,,/Resources/noconnect.png"));
                    }));
                    proxy[indexPc] = null;
                    _timerGetImage[indexPc].Stop();

                }
                GC.Collect();
            }
        }
        /// <summary>
        /// Трансляция источника
        /// </summary>
        public ICommand ClickCommand { get; set; }
        public ICommand OpenSiteCommand { get; set; }
        /// <summary>
        /// Просмотор и управления отдельным источником
        /// </summary>
        public ICommand OpenWindowCommand { get; set; }
        /// <summary>
        /// Закрытие приложения
        /// </summary>
        public ICommand CloseAppCommand { get; set; }
        /// <summary>
        /// ToDo
        /// </summary>
        public ICommand ToDoCommand { get; set; }
        /// <summary>
        /// Отправка сообщения выбранному источнику
        /// </summary>
        public ICommand SendMessageCommand { get; set; }
        /// <summary>
        /// Выключение выбранного источника
        /// </summary>
        public ICommand ShutDownCommand { get; set; }
        /// <summary>
        /// Перезагрузка выбранного источника
        /// </summary>
        public ICommand RestartCommand { get; set; }
        /// <summary>
        /// Выход из системы выбранного источника
        /// </summary>
        public ICommand LogOffCommand { get; set; }


        void callback_GetImageEven(object sender, CallbackEventArgs e)
        {
            
            BitmapImage partial;
            Int32Rect bounds;
            byte[] imgData;
            Win32Stuff.RESULTION resultion;
            if (e.ImageBytes != null)
            {
               
               Function.UnpackImage(e.ImageBytes, out partial, out bounds, out imgData, out resultion);
               WriteableBitmap test = oldImage[e.IndexPc];
               Function.UpdateScreen(ref test, partial, bounds, imgData, resultion);
                oldImage[e.IndexPc] = test;
                test.Freeze();
                GetPc[e.IndexPc].Image = oldImage[e.IndexPc];
                GC.Collect();
            }
        }

        public static string[] GetAvailableAddresses()
        {
            using (DiscoveryClient client = new DiscoveryClient(new UdpDiscoveryEndpoint()))
            {
                Collection<EndpointDiscoveryMetadata> endpointsMetadata = client.Find(new FindCriteria(typeof(IScreen))).Endpoints;
                if (endpointsMetadata != null && endpointsMetadata.Count != 0)
                {
                    return endpointsMetadata.Select(ep => ep.Address.Uri.Host).ToArray();
                }
                return null;
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
